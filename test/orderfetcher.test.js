"use strict";

const {
    expect
} = require("chai");
const Big = require("big.js");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const orderfetcher = require("../lib/orderfetcher.js");

describe("OrderFetcher", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("SimulationOrderFetcher", () => {
        it("Constructor should succeed with no parameters", () => {
            const orderFetcher = new orderfetcher.SimulationOrderFetcher();
            expect(orderFetcher.constructor.name).to.equal("SimulationOrderFetcher");
        });

        it("fetchExistingOrders() should return an empty list", async () => {
            const orderFetcher = new orderfetcher.SimulationOrderFetcher();
            const actual = await orderFetcher.fetchExistingOrders();
            expect(actual.length).to.equal(0);
        });
    });

    describe("SyncOrderFetcher", () => {
        it("Constructor should succeed with valid parameters", () => {
            const orderFetcher = new orderfetcher.SyncOrderFetcher(fakes.context(), fakes.market(), fakes.owner());
            expect(orderFetcher.constructor.name).to.equal("SyncOrderFetcher");
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new orderfetcher.SyncOrderFetcher({}, fakes.market(), fakes.owner());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new orderfetcher.SyncOrderFetcher(fakes.context(), {}, fakes.owner());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid owner", () => {
            const shouldThrow = () => new orderfetcher.SyncOrderFetcher(fakes.context(), fakes.market(), {});
            expect(shouldThrow).to.throw();
        });

        it("fetchExistingOrders() should return correct orders", async () => {
            const orderFetcher = new orderfetcher.SyncOrderFetcher(fakes.context(), fakes.market(), fakes.owner());
            const actual = await orderFetcher.fetchExistingOrders();

            expect(actual[0].clientId.eq(new Big(201))).to.be.true;
            expect(actual[1].clientId.eq(new Big(202))).to.be.true;
        });
    });
});