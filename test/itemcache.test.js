"use strict";

const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const {ItemCache} = require("../lib/itemcache.js");
const log = require("../lib/log.js");

describe("ItemCache", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    const FAKE_ADDRESS_1 = fakes.randomPublicKey();
    const FAKE_ADDRESS_2 = fakes.randomPublicKey();
    const FAKE_ADDRESS_3 = fakes.randomPublicKey();
    const UNKNOWN_ADDRESS = fakes.randomPublicKey();

    class TestItem {
        constructor(symbol, address) {
            this.symbol = symbol;
            this.address = address;
        }
    }

    const UnknownItem = new TestItem("Unknown", UNKNOWN_ADDRESS);

    function createTestItemCache() {
        const item1 = new TestItem("Test1", FAKE_ADDRESS_1);
        const item2 = new TestItem("Test2", FAKE_ADDRESS_2);
        return new ItemCache(TestItem, [item1, item2], UnknownItem);
    }

    describe("ItemCache", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const item = new TestItem("Test1", FAKE_ADDRESS_1);
            const actual = new ItemCache(TestItem, [item]);
            expect(actual.constructor.name).to.equal("ItemCache");
            expect(actual.isItemCache).to.be.true;
        });

        it("Constructor should successfully construct with empty item array", () => {
            const actual = new ItemCache(TestItem, []);
            expect(actual.constructor.name).to.equal("ItemCache");
            expect(actual.isItemCache).to.be.true;
        });

        it("Constructor should throw with invalid item type", () => {
            const shouldThrow = () => new ItemCache("invalid", []);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid item array", () => {
            const shouldThrow = () => new ItemCache(TestItem, "invalid");
            expect(shouldThrow).to.throw();
        });

        it("bySymbol() should find Test1 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.bySymbol("Test1");
            expect(item.symbol).to.equal("Test1");
            expect(FAKE_ADDRESS_1.equals(item.address)).to.be.true;
        });

        it("bySymbol() should find Test2 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.bySymbol("Test2");
            expect(item.symbol).to.equal("Test2");
            expect(FAKE_ADDRESS_2.equals(item.address)).to.be.true;
        });

        it("bySymbol() should find TEST1 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.bySymbol("TEST1");
            expect(item.symbol).to.equal("Test1");
            expect(FAKE_ADDRESS_1.equals(item.address)).to.be.true;
        });

        it("bySymbol() should throw for invalid parameter type", () => {
            const itemCache = createTestItemCache();
            const shouldThrow = () => itemCache.bySymbol({});
            expect(shouldThrow).to.throw();
        });

        it("bySymbol() should throw for unknown symbol", () => {
            const itemCache = createTestItemCache();
            const shouldThrow = () => itemCache.bySymbol("QWERTY");
            expect(shouldThrow).to.throw();
        });

        it("bySymbolOrUnknown() should find Test1 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.bySymbolOrUnknown("Test1");
            expect(item.symbol).to.equal("Test1");
            expect(FAKE_ADDRESS_1.equals(item.address)).to.be.true;
        });

        it("bySymbolOrUnknown() should find Test2 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.bySymbolOrUnknown("Test2");
            expect(item.symbol).to.equal("Test2");
            expect(FAKE_ADDRESS_2.equals(item.address)).to.be.true;
        });

        it("bySymbolOrUnknown() should find TEST1 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.bySymbolOrUnknown("TEST1");
            expect(item.symbol).to.equal("Test1");
            expect(FAKE_ADDRESS_1.equals(item.address)).to.be.true;
        });

        it("bySymbolOrUnknown() should throw for invalid address parameter type", () => {
            const itemCache = createTestItemCache();
            const shouldThrow = () => itemCache.bySymbolOrUnknown({});
            expect(shouldThrow).to.throw();
        });

        it("bySymbolOrUnknown() should return UnknownItem for unknown symbol", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.bySymbolOrUnknown("QWERTY");
            expect(item).to.equal(UnknownItem);
        });

        it("bySymbolOrUnknown() should return undefined for unknown symbol when no unspecified unknown", () => {
            const item1 = new TestItem("Test1", FAKE_ADDRESS_1);
            const item2 = new TestItem("Test2", FAKE_ADDRESS_2);
            const itemCache = new ItemCache(TestItem, [item1, item2]);
            const item = itemCache.bySymbolOrUnknown("QWERTY");
            // eslint-disable-next-line no-undefined
            expect(item).to.equal(undefined);
        });

        it("byAddress() should find Test1 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.byAddress(FAKE_ADDRESS_1);
            expect(item.symbol).to.equal("Test1");
            expect(FAKE_ADDRESS_1.equals(item.address)).to.be.true;
        });

        it("byAddress() should find Test2 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.byAddress(FAKE_ADDRESS_2);
            expect(item.symbol).to.equal("Test2");
            expect(FAKE_ADDRESS_2.equals(item.address)).to.be.true;
        });

        it("byAddress() should throw for invalid address parameter type", () => {
            const itemCache = createTestItemCache();
            const shouldThrow = () => itemCache.byAddress({});
            expect(shouldThrow).to.throw();
        });

        it("byAddress() should throw for unknown address", () => {
            const itemCache = createTestItemCache();
            const shouldThrow = () => itemCache.byAddress(fakes.randomPublicKey());
            expect(shouldThrow).to.throw();
        });

        it("byAddressOrUnknown() should find Test1 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.byAddressOrUnknown(FAKE_ADDRESS_1);
            expect(item.symbol).to.equal("Test1");
            expect(FAKE_ADDRESS_1.equals(item.address)).to.be.true;
        });

        it("byAddressOrUnknown() should find Test2 item", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.byAddressOrUnknown(FAKE_ADDRESS_2);
            expect(item.symbol).to.equal("Test2");
            expect(FAKE_ADDRESS_2.equals(item.address)).to.be.true;
        });

        it("byAddressOrUnknown() should throw for invalid address parameter type", () => {
            const itemCache = createTestItemCache();
            const shouldThrow = () => itemCache.byAddressOrUnknown({});
            expect(shouldThrow).to.throw();
        });

        it("byAddressOrUnknown() should return UnknownItem for unknown address", () => {
            const itemCache = createTestItemCache();
            const item = itemCache.byAddressOrUnknown(fakes.randomPublicKey());
            expect(item).to.equal(UnknownItem);
        });

        it("byAddressOrUnknown() should return undefined for unknown address when no unspecified unknown", () => {
            const item1 = new TestItem("Test1", FAKE_ADDRESS_1);
            const item2 = new TestItem("Test2", FAKE_ADDRESS_2);
            const itemCache = new ItemCache(TestItem, [item1, item2]);
            const item = itemCache.byAddressOrUnknown(fakes.randomPublicKey());
            // eslint-disable-next-line no-undefined
            expect(item).to.equal(undefined);
        });

        it("add() should throw for invalid parameter type", () => {
            const itemCache = createTestItemCache();
            const shouldThrow = () => itemCache.add({});
            expect(shouldThrow).to.throw();
        });

        it("add() should succeed with new item", () => {
            const itemCache = createTestItemCache();
            const item = new TestItem("Test3", FAKE_ADDRESS_3);
            const shouldNotExist = itemCache.bySymbolOrUnknown(item.symbol);
            expect(shouldNotExist).to.equal(UnknownItem);
            itemCache.add(item);
            const shouldExist = itemCache.bySymbolOrUnknown(item.symbol);
            expect(shouldExist).to.equal(item);
        });
    });
});