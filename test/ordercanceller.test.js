"use strict";

const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const ordercanceller = require("../lib/ordercanceller.js");

describe("OrderCanceller", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("SimulationOrderCanceller", () => {
        it("Constructor should succeed with no parameters", () => {
            const orderCanceller = new ordercanceller.SimulationOrderCanceller();
            expect(orderCanceller.constructor.name).to.equal("SimulationOrderCanceller");
        });

        it("cancelOrders() should not cancel orders", async () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderCanceller = new ordercanceller.SimulationOrderCanceller();
            await orderCanceller.cancelOrders(fakes.orders());
            expect(market.typedCancelOrder.called).to.be.false;
            expect(context.connection.waitFor.called).to.be.false;
        });
    });

    describe("AsyncOrderCanceller", () => {
        it("Constructor should succeed with valid parameters", () => {
            const orderCanceller = new ordercanceller.AsyncOrderCanceller(
                fakes.context(),
                fakes.market(),
                fakes.owner()
            );
            expect(orderCanceller.constructor.name).to.equal("AsyncOrderCanceller");
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new ordercanceller.AsyncOrderCanceller({}, fakes.market(), fakes.owner());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new ordercanceller.AsyncOrderCanceller(fakes.context(), {}, fakes.owner());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid owner", () => {
            const shouldThrow = () => new ordercanceller.AsyncOrderCanceller(fakes.context(), fakes.market(), {});
            expect(shouldThrow).to.throw();
        });

        it("cancelOrders() should cancel orders but not wait", async () => {
            const context = fakes.context();
            const market = fakes.market();
            const owner = fakes.owner();
            const orderCanceller = new ordercanceller.AsyncOrderCanceller(
                context,
                market,
                owner
            );
            await orderCanceller.cancelOrders(fakes.orders());
            expect(market.typedCancelOrder.called).to.be.true;
            expect(market.typedCancelOrder.calledTwice).to.be.true;
            expect(context.connection.waitFor.called).to.be.false;
        });
    });

    describe("AsyncWaitingOrderCanceller", () => {
        it("Constructor should succeed with valid parameters", () => {
            const orderCanceller = new ordercanceller.AsyncWaitingOrderCanceller(
                fakes.context(),
                fakes.market(),
                fakes.owner()
            );
            expect(orderCanceller.constructor.name).to.equal("AsyncWaitingOrderCanceller");
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new ordercanceller.AsyncWaitingOrderCanceller(
                {},
                fakes.market(),
                fakes.owner()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new ordercanceller.AsyncWaitingOrderCanceller(
                fakes.context(),
                {},
                fakes.owner()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid owner", () => {
            const shouldThrow = () => new ordercanceller.AsyncWaitingOrderCanceller(
                fakes.context(),
                fakes.market(),
                {}
            );
            expect(shouldThrow).to.throw();
        });

        it("cancelOrders() should cancel orders and wait", async () => {
            const context = fakes.context();
            const market = fakes.market();
            const owner = fakes.owner();
            const orderCanceller = new ordercanceller.AsyncWaitingOrderCanceller(
                context,
                market,
                owner
            );
            await orderCanceller.cancelOrders(fakes.orders());
            expect(market.typedCancelOrder.called).to.be.true;
            expect(market.typedCancelOrder.calledTwice).to.be.true;
            expect(context.connection.waitFor.called).to.be.true;
            expect(context.connection.waitFor.calledTwice).to.be.true;
        });
    });

    describe("SyncOrderCanceller", () => {
        it("Constructor should succeed with no parameters", () => {
            const orderCanceller = new ordercanceller.SyncOrderCanceller(
                fakes.context(),
                fakes.market(),
                fakes.owner()
            );
            expect(orderCanceller.constructor.name).to.equal("SyncOrderCanceller");
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new ordercanceller.SyncOrderCanceller({}, fakes.market(), fakes.owner());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new ordercanceller.SyncOrderCanceller(fakes.context(), {}, fakes.owner());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid owner", () => {
            const shouldThrow = () => new ordercanceller.SyncOrderCanceller(fakes.context(), fakes.market(), {});
            expect(shouldThrow).to.throw();
        });

        it("cancelOrders() should cancel orders and wait", async () => {
            const context = fakes.context();
            const market = fakes.market();
            const owner = fakes.owner();
            const orderCanceller = new ordercanceller.SyncOrderCanceller(context, market, owner);
            await orderCanceller.cancelOrders(fakes.orders());
            expect(market.typedCancelOrder.called).to.be.true;
            expect(market.typedCancelOrder.calledTwice).to.be.true;
            expect(context.connection.waitFor.called).to.be.true;
            expect(context.connection.waitFor.calledTwice).to.be.true;
        });
    });
});