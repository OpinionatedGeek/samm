"use strict";

const accounttracker = require("../lib/accounttracker.js");
const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");

describe("AccountTracker", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("SimulationAccountTracker", () => {
        const tokenAccounts = [fakes.tokenAccount()];

        it("Constructor should fail with no parameters", () => {
            const shouldThrow = () => new accounttracker.SimulationAccountTracker();
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid base token account parameter", () => {
            const shouldThrow = () => new accounttracker.SimulationAccountTracker("invalid", tokenAccounts, false);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid quote token account parameter", () => {
            const shouldThrow = () => new accounttracker.SimulationAccountTracker(tokenAccounts, "invalid", false);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid result parameter", () => {
            const shouldThrow = () => new accounttracker.SimulationAccountTracker(tokenAccounts, tokenAccounts);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should succeed with valid parameters", () => {
            const accountTracker = new accounttracker.SimulationAccountTracker(tokenAccounts, tokenAccounts, false);
            expect(accountTracker.constructor.name).to.equal("SimulationAccountTracker");
            expect(accountTracker.isAccountTracker).to.be.true;
        });

        it("isOneOfMyOpenOrdersAccounts() should return false when constructed with false parameter", async () => {
            const accountTracker = new accounttracker.SimulationAccountTracker(tokenAccounts, tokenAccounts, false);
            expect(await accountTracker.isOneOfMyOpenOrdersAccounts()).to.be.false;
        });

        it("isOneOfMyOpenOrdersAccounts() should return true when constructed with true parameter", async () => {
            const accountTracker = new accounttracker.SimulationAccountTracker(tokenAccounts, tokenAccounts, true);
            expect(await accountTracker.isOneOfMyOpenOrdersAccounts()).to.be.true;
        });
    });

    describe("SyncAccountTracker", () => {
        const openOrdersAccounts = [fakes.openOrders(), fakes.openOrders()];
        const baseTokenAccounts = [fakes.tokenAccount(), fakes.tokenAccount()];
        const quoteTokenAccounts = [fakes.tokenAccount(), fakes.tokenAccount()];

        it("Constructor should fail with no parameters", () => {
            const shouldThrow = () => new accounttracker.SyncAccountTracker();
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid base token account parameter", () => {
            const shouldThrow = () => new accounttracker.SyncAccountTracker(
                "invalid",
                quoteTokenAccounts,
                openOrdersAccounts
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid quote token account parameter", () => {
            const shouldThrow = () => new accounttracker.SyncAccountTracker(
                baseTokenAccounts,
                "invalid",
                openOrdersAccounts
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid open orders account parameter", () => {
            const shouldThrow = () => new accounttracker.SyncAccountTracker(
                baseTokenAccounts,
                quoteTokenAccounts,
                "invalid"
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should successfully construct with valid parameters", () => {
            const accountTracker = new accounttracker.SyncAccountTracker(
                baseTokenAccounts,
                quoteTokenAccounts,
                openOrdersAccounts
            );
            expect(accountTracker.constructor.name).to.equal("SyncAccountTracker");
            expect(accountTracker.isAccountTracker).to.be.true;
        });

        it("isOneOfMyOpenOrdersAccounts() should return true for my first PublicKey", () => {
            const accounts = [fakes.openOrders(), fakes.openOrders()];
            const accountTracker = new accounttracker.SyncAccountTracker(
                baseTokenAccounts,
                quoteTokenAccounts,
                accounts
            );
            expect(accountTracker.isOneOfMyOpenOrdersAccounts(accounts[0].address)).to.be.true;
        });

        it("isOneOfMyOpenOrdersAccounts() should return true for my second PublicKey", () => {
            const accounts = [fakes.openOrders(), fakes.openOrders()];
            const accountTracker = new accounttracker.SyncAccountTracker(
                baseTokenAccounts,
                quoteTokenAccounts,
                accounts
            );
            expect(accountTracker.isOneOfMyOpenOrdersAccounts(accounts[1].address)).to.be.true;
        });

        it("isOneOfMyOpenOrdersAccounts() should return false for different PublicKey", () => {
            const accountTracker = new accounttracker.SyncAccountTracker(
                baseTokenAccounts,
                quoteTokenAccounts,
                openOrdersAccounts
            );
            expect(accountTracker.isOneOfMyOpenOrdersAccounts(fakes.openOrders().address)).to.be.false;
        });
    });
});