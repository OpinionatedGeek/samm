"use strict";

const {
    expect
} = require("chai");
const array = require("../lib/array.js");
const log = require("../lib/log.js");
const sinon = require("sinon");

describe("Array", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    // For tests that take an object with an eq() method.
    class Eqer {
        constructor(val) {
            this.val = val;
        }

        eq(otherVal) {
            return this.val === otherVal.val;
        }
    }

    // For tests that take an object with an equals() method.
    class Equaler {
        constructor(val) {
            this.val = val;
        }

        equals(otherVal) {
            return this.val === otherVal.val;
        }
    }

    describe("find()", () => {
        it("find() should throw with invalid array", () => {
            const shouldThrow = () => array.find({}, new Eqer(1));
            expect(shouldThrow).to.throw();
        });

        it("find() should throw with invalid array, irrespective of equality test", () => {
            const shouldThrow = () => array.find({}, new Eqer(1), "eq");
            expect(shouldThrow).to.throw();
        });

        it("find() should find first entry", () => {
            const testArray = [1, 2, 3];
            const actual = array.find(testArray, 1);
            expect(actual).to.equal(0);
        });

        it("find() should find middle entry", () => {
            const testArray = [1, 2, 3];
            const actual = array.find(testArray, 2);
            expect(actual).to.equal(1);
        });

        it("find() should find last entry", () => {
            const testArray = [1, 2, 3];
            const actual = array.find(testArray, 3);
            expect(actual).to.equal(2);
        });

        it("find() should not find nonexistant entry", () => {
            const testArray = [1, 2, 3];
            const actual = array.find(testArray, 4);
            expect(actual).to.equal(-1);
        });

        it("find() should find first entry using equality test", () => {
            const testArray = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.find(testArray, new Eqer(1), "eq");
            expect(actual).to.equal(0);
        });

        it("find() should find middle entry using equality test", () => {
            const testArray = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.find(testArray, new Eqer(2), "eq");
            expect(actual).to.equal(1);
        });

        it("find() should find last entry using equality test", () => {
            const testArray = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.find(testArray, new Eqer(3), "eq");
            expect(actual).to.equal(2);
        });

        it("find() should not find nonexistant entry using equality test", () => {
            const testArray = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.find(testArray, new Eqer(4), "eq");
            expect(actual).to.equal(-1);
        });
    });

    describe("has()", () => {
        it("has() should throw with invalid array", () => {
            const shouldThrow = () => array.has({}, new Eqer(1));
            expect(shouldThrow).to.throw();
        });

        it("has() should throw with invalid array, irrespective of equality test", () => {
            const shouldThrow = () => array.has({}, new Eqer(1), "eq");
            expect(shouldThrow).to.throw();
        });

        it("has() should find first entry", () => {
            const testArray = [1, 2, 3];
            const actual = array.has(testArray, 1);
            expect(actual).to.be.true;
        });

        it("has() should find middle entry", () => {
            const testArray = [1, 2, 3];
            const actual = array.has(testArray, 2);
            expect(actual).to.be.true;
        });

        it("has() should find last entry", () => {
            const testArray = [1, 2, 3];
            const actual = array.has(testArray, 3);
            expect(actual).to.be.true;
        });

        it("has() should not find nonexistant entry", () => {
            const testArray = [1, 2, 3];
            const actual = array.has(testArray, 4);
            expect(actual).to.be.false;
        });

        it("has() should find first entry using equality test", () => {
            const testArray = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.has(testArray, new Eqer(1), "eq");
            expect(actual).to.be.true;
        });

        it("has() should find middle entry using equality test", () => {
            const testArray = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.has(testArray, new Eqer(2), "eq");
            expect(actual).to.be.true;
        });

        it("has() should find last entry using equality test", () => {
            const testArray = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.has(testArray, new Eqer(3), "eq");
            expect(actual).to.be.true;
        });

        it("has() should not find nonexistant entry using equality test", () => {
            const testArray = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.has(testArray, new Eqer(4), "eq");
            expect(actual).to.be.false;
        });
    });

    describe("hasAllMembersOrderDependent() with reference equality", () => {
        it("hasAllMembersOrderDependent() should throw with invalid first array", () => {
            const shouldThrow = () => array.hasAllMembersOrderDependent({}, [1]);
            expect(shouldThrow).to.throw();
        });

        it("hasAllMembersOrderDependent() should throw with invalid second array", () => {
            const shouldThrow = () => array.hasAllMembersOrderDependent([1], {});
            expect(shouldThrow).to.throw();
        });

        it("hasAllMembersOrderDependent() should succeed with empty arrays", () => {
            const testArray1 = [];
            const testArray2 = [];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2);
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderDependent() should succeed with identical members", () => {
            const testArray1 = [1, 2, 3];
            const testArray2 = [1, 2, 3];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2);
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderDependent() should fail with reversed members", () => {
            const testArray1 = [1, 2, 3];
            const testArray2 = [3, 2, 1];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2);
            expect(actual).to.be.false;
        });

        it("hasAllMembersOrderDependent() should fail with mixed-up members", () => {
            const testArray1 = [1, 2, 3];
            const testArray2 = [2, 3, 1];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2);
            expect(actual).to.be.false;
        });

        it("hasAllMembersOrderDependent() should fail with shorter first array", () => {
            const testArray1 = [1, 2];
            const testArray2 = [1, 2, 3];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2);
            expect(actual).to.be.false;
        });

        it("hasAllMembersOrderDependent() should fail with shorter second array", () => {
            const testArray1 = [1, 2, 3];
            const testArray2 = [1, 2];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2);
            expect(actual).to.be.false;
        });
    });

    describe("hasAllMembersOrderDependent() with equality test", () => {
        it("hasAllMembersOrderDependent() should throw with invalid first array", () => {
            const shouldThrow = () => array.hasAllMembersOrderDependent({}, [new Eqer(1)], "eq");
            expect(shouldThrow).to.throw();
        });

        it("hasAllMembersOrderDependent() should throw with invalid second array", () => {
            const shouldThrow = () => array.hasAllMembersOrderDependent([new Eqer(1)], {}, "eq");
            expect(shouldThrow).to.throw();
        });

        it("hasAllMembersOrderDependent() should succeed with empty arrays", () => {
            const testArray1 = [];
            const testArray2 = [];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2, "eq");
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderDependent() should succeed with identical members", () => {
            const testArray1 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const testArray2 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2, "eq");
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderDependent() should fail with reversed members", () => {
            const testArray1 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const testArray2 = [new Eqer(3), new Eqer(2), new Eqer(1)];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2, "eq");
            expect(actual).to.be.false;
        });

        it("hasAllMembersOrderDependent() should fail with mixed-up members", () => {
            const testArray1 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const testArray2 = [new Eqer(2), new Eqer(3), new Eqer(1)];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2, "eq");
            expect(actual).to.be.false;
        });

        it("hasAllMembersOrderDependent() should fail with shorter first array", () => {
            const testArray1 = [new Eqer(1), new Eqer(2)];
            const testArray2 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2, "eq");
            expect(actual).to.be.false;
        });

        it("hasAllMembersOrderDependent() should fail with shorter second array", () => {
            const testArray1 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const testArray2 = [new Eqer(1), new Eqer(2)];
            const actual = array.hasAllMembersOrderDependent(testArray1, testArray2, "eq");
            expect(actual).to.be.false;
        });
    });

    describe("hasAllMembersOrderIndependent() with reference equality", () => {
        it("hasAllMembersOrderIndependent() should throw with invalid first array", () => {
            const shouldThrow = () => array.hasAllMembersOrderIndependent({}, [1]);
            expect(shouldThrow).to.throw();
        });

        it("hasAllMembersOrderIndependent() should throw with invalid second array", () => {
            const shouldThrow = () => array.hasAllMembersOrderIndependent([1], {});
            expect(shouldThrow).to.throw();
        });

        it("hasAllMembersOrderIndependent() should succeed with empty arrays", () => {
            const testArray1 = [];
            const testArray2 = [];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2);
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderIndependent() should succeed with identical members", () => {
            const testArray1 = [1, 2, 3];
            const testArray2 = [1, 2, 3];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2);
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderIndependent() should succeed with reversed members", () => {
            const testArray1 = [1, 2, 3];
            const testArray2 = [3, 2, 1];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2);
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderIndependent() should succeed with mixed-up members", () => {
            const testArray1 = [1, 2, 3];
            const testArray2 = [2, 3, 1];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2);
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderIndependent() should fail with shorter first array", () => {
            const testArray1 = [1, 2];
            const testArray2 = [1, 2, 3];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2);
            expect(actual).to.be.false;
        });

        it("hasAllMembersOrderIndependent() should fail with shorter second array", () => {
            const testArray1 = [1, 2, 3];
            const testArray2 = [1, 2];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2);
            expect(actual).to.be.false;
        });
    });

    describe("hasAllMembersOrderIndependent() with equality test", () => {
        it("hasAllMembersOrderIndependent() should throw with invalid first array", () => {
            const shouldThrow = () => array.hasAllMembersOrderIndependent({}, [new Eqer(1)], "eq");
            expect(shouldThrow).to.throw();
        });

        it("hasAllMembersOrderIndependent() should throw with invalid second array", () => {
            const shouldThrow = () => array.hasAllMembersOrderIndependent([new Eqer(1)], {}, "eq");
            expect(shouldThrow).to.throw();
        });

        it("hasAllMembersOrderIndependent() should succeed with empty arrays", () => {
            const testArray1 = [];
            const testArray2 = [];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2, "eq");
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderIndependent() should succeed with identical members", () => {
            const testArray1 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const testArray2 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2, "eq");
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderIndependent() should succeed with reversed members", () => {
            const testArray1 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const testArray2 = [new Eqer(3), new Eqer(2), new Eqer(1)];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2, "eq");
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderIndependent() should succeed with mixed-up members", () => {
            const testArray1 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const testArray2 = [new Eqer(2), new Eqer(3), new Eqer(1)];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2, "eq");
            expect(actual).to.be.true;
        });

        it("hasAllMembersOrderIndependent() should fail with shorter first array", () => {
            const testArray1 = [new Eqer(1), new Eqer(2)];
            const testArray2 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2, "eq");
            expect(actual).to.be.false;
        });

        it("hasAllMembersOrderIndependent() should fail with shorter second array", () => {
            const testArray1 = [new Eqer(1), new Eqer(2), new Eqer(3)];
            const testArray2 = [new Eqer(1), new Eqer(2)];
            const actual = array.hasAllMembersOrderIndependent(testArray1, testArray2, "eq");
            expect(actual).to.be.false;
        });
    });

    describe("insertAtIndex()", () => {
        it("insertAtIndex() should throw with invalid array", () => {
            const shouldThrow = () => array.insertAtIndex({}, 1, 1);
            expect(shouldThrow).to.throw();
        });

        it("insertAtIndex() should throw with invalid index", () => {
            const shouldThrow = () => array.insertAtIndex([1], 2, 1);
            expect(shouldThrow).to.throw();
        });

        it("insertAtIndex() should add entry to head of empty array", () => {
            const actual = [];
            array.insertAtIndex(actual, 0, 1);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(1)
                .that.has.members([1]);
        });

        it("insertAtIndex() should add entry to head of non-empty array", () => {
            const actual = [1, 2, 3];
            array.insertAtIndex(actual, 0, 4);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(4)
                .that.has.members([1, 2, 3, 4]);
            expect(actual[0]).to.equal(4);
        });

        it("insertAtIndex() should add after first entry", () => {
            const actual = [1, 2, 3];
            array.insertAtIndex(actual, 1, 4);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(4)
                .that.has.members([1, 2, 3, 4]);
            expect(actual[1]).to.equal(4);
        });

        it("insertAtIndex() should add after last entry", () => {
            const actual = [1, 2, 3];
            array.insertAtIndex(actual, 3, 4);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(4)
                .that.has.members([1, 2, 3, 4]);
            expect(actual[3]).to.equal(4);
        });
    });

    describe("insertAtHead()", () => {
        it("insertAtHead() should throw with invalid array", () => {
            const shouldThrow = () => array.insertAtHead({}, 1);
            expect(shouldThrow).to.throw();
        });

        it("insertAtHead() should add entry to head of empty array", () => {
            const actual = [];
            array.insertAtHead(actual, 1);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(1)
                .that.has.members([1]);
        });

        it("insertAtHead() should add entry to head of non-empty array", () => {
            const actual = [1, 2, 3];
            array.insertAtHead(actual, 4);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(4)
                .that.has.members([1, 2, 3, 4]);
            expect(actual[0]).to.equal(4);
        });
    });

    describe("removeIndex()", () => {
        it("removeIndex() should throw with invalid array", () => {
            const shouldThrow = () => array.removeIndex({}, 1);
            expect(shouldThrow).to.throw();
        });

        it("removeIndex() should throw with invalid index", () => {
            const shouldThrow = () => array.removeIndex([1], 1);
            expect(shouldThrow).to.throw();
        });

        it("removeIndex() should remove single entry", () => {
            const actual = [1];
            array.removeIndex(actual, 0);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(0);
        });

        it("removeIndex() should remove first entry", () => {
            const actual = [1, 2, 3];
            array.removeIndex(actual, 0);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.members([2, 3])
                .and.not.members([1]);
        });

        it("removeIndex() should remove middle entry", () => {
            const actual = [1, 2, 3];
            array.removeIndex(actual, 1);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.members([1, 3])
                .and.not.members([2]);
        });

        it("removeIndex() should remove last entry", () => {
            const actual = [1, 2, 3];
            array.removeIndex(actual, 2);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.members([1, 2])
                .and.not.members([2]);
        });
    });

    describe("remove() no equality test", () => {
        it("remove() should throw with invalid array", () => {
            const shouldThrow = () => array.remove({}, 1);
            expect(shouldThrow).to.throw();
        });

        it("remove() should remove single entry", () => {
            const actual = [1];
            array.remove(actual, 1);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(0);
        });

        it("remove() should remove first entry", () => {
            const actual = [1, 2, 3];
            array.remove(actual, 1);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.members([2, 3])
                .and.not.members([1]);
        });

        it("remove() should remove middle entry", () => {
            const actual = [1, 2, 3];
            array.remove(actual, 2);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.members([1, 3])
                .and.not.members([2]);
        });

        it("remove() should remove last entry", () => {
            const actual = [1, 2, 3];
            array.remove(actual, 3);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.members([1, 2])
                .and.not.members([2]);
        });

        it("remove() should remove middle string", () => {
            const actual = ["1", "2", "3"];
            array.remove(actual, "2");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.members(["1", "3"])
                .and.not.members(["2"]);
        });

        it("remove() should remove undefined", () => {
            // eslint-disable-next-line no-undefined
            const actual = ["1", undefined, "3"];
            // eslint-disable-next-line no-undefined
            array.remove(actual, undefined);
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.members(["1", "3"]);
        });

        it("remove() should leave undefined if present", () => {
            // eslint-disable-next-line no-undefined
            const actual = ["1", undefined, "3"];
            // eslint-disable-next-line no-undefined
            array.remove(actual, "3");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                // eslint-disable-next-line no-undefined
                .that.has.members(["1", undefined])
                .and.not.members(["3"]);
        });
    });

    describe("remove() with 'equals' test specified", () => {
        it("remove() should throw with invalid array", () => {
            const shouldThrow = () => array.remove({}, new Equaler(1), "equals");
            expect(shouldThrow).to.throw();
        });

        it("remove() should remove single entry", () => {
            const actual = [new Equaler(1)];
            array.remove(actual, new Equaler(1), "equals");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(0);
        });

        it("remove() should remove first entry", () => {
            const actual = [new Equaler(1), new Equaler(2), new Equaler(3)];
            array.remove(actual, new Equaler(1), "equals");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.deep.members([new Equaler(2), new Equaler(3)])
                .and.not.deep.members([new Equaler(1)]);
        });

        it("remove() should remove middle entry", () => {
            const actual = [new Equaler(1), new Equaler(2), new Equaler(3)];
            array.remove(actual, new Equaler(2), "equals");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.deep.members([new Equaler(1), new Equaler(3)])
                .and.not.deep.members([new Equaler(2)]);
        });

        it("remove() should remove last entry", () => {
            const actual = [new Equaler(1), new Equaler(2), new Equaler(3)];
            array.remove(actual, new Equaler(3), "equals");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.deep.members([new Equaler(1), new Equaler(2)])
                .and.not.deep.members([new Equaler(3)]);
        });
    });

    describe("remove() with 'eq' test specified", () => {
        it("remove() should throw with invalid array", () => {
            const shouldThrow = () => array.remove({}, new Eqer(1), "eq");
            expect(shouldThrow).to.throw();
        });

        it("remove() should remove single entry", () => {
            const actual = [new Eqer(1)];
            array.remove(actual, new Eqer(1), "eq");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(0);
        });

        it("remove() should remove first entry", () => {
            const actual = [new Eqer(1), new Eqer(2), new Eqer(3)];
            array.remove(actual, new Eqer(1), "eq");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.deep.members([new Eqer(2), new Eqer(3)])
                .and.not.deep.members([new Eqer(1)]);
        });

        it("remove() should remove middle entry", () => {
            const actual = [new Eqer(1), new Eqer(2), new Eqer(3)];
            array.remove(actual, new Eqer(2), "eq");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.deep.members([new Eqer(1), new Eqer(3)])
                .and.not.deep.members([new Eqer(2)]);
        });

        it("remove() should remove last entry", () => {
            const actual = [new Eqer(1), new Eqer(2), new Eqer(3)];
            array.remove(actual, new Eqer(3), "eq");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(2)
                .that.has.deep.members([new Eqer(1), new Eqer(2)])
                .and.not.deep.members([new Eqer(3)]);
        });
    });

    describe("CompoundItem", () => {
        class TestItem {
            constructor() {
                this.compoundMethod = sinon.spy();
            }
        }

        it("Constructor should successfully construct with no items", () => {
            const actual = new array.CompoundItem(TestItem, "compoundMethod");
            expect(actual.constructor.name).to.equal("CompoundItem");
            expect(actual.items).to.be.an("array")
                .to.have.lengthOf(0);
        });

        it("Constructor should successfully construct with single item", () => {
            const actual = new array.CompoundItem(TestItem, "compoundMethod", new TestItem());
            expect(actual.constructor.name).to.equal("CompoundItem");
            expect(actual.items).to.be.an("array")
                .to.have.lengthOf(1);
        });

        it("Constructor should successfully construct with two items", () => {
            const actual = new array.CompoundItem(TestItem, "compoundMethod", new TestItem(), new TestItem());
            expect(actual.constructor.name).to.equal("CompoundItem");
            expect(actual.items).to.be.an("array")
                .to.have.lengthOf(2);
        });

        it("Constructor should throw with invalid type parameter", () => {
            const shouldThrow = () => new array.CompoundItem({}, "compoundMethod", new TestItem());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid function name parameter", () => {
            const shouldThrow = () => new array.CompoundItem(TestItem, {}, new TestItem());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with if first item isn't a TestItem", () => {
            const shouldThrow = () => new array.CompoundItem(TestItem, "compoundMethod", {}, new TestItem());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with if second item isn't a TestItem", () => {
            const shouldThrow = () => new array.CompoundItem(TestItem, "compoundMethod", new TestItem(), {});
            expect(shouldThrow).to.throw();
        });

        it("compoundMethod() should call compoundMethod() in each item", () => {
            const item1 = new TestItem();
            const item2 = new TestItem();
            const item3 = new TestItem();
            const actual = new array.CompoundItem(TestItem, "compoundMethod", item1, item2, item3);

            expect(item1.compoundMethod.calledOnce).to.be.false;
            expect(item2.compoundMethod.calledOnce).to.be.false;
            expect(item3.compoundMethod.calledOnce).to.be.false;

            actual.compoundMethod({});

            expect(item1.compoundMethod.calledOnce).to.be.true;
            expect(item2.compoundMethod.calledOnce).to.be.true;
            expect(item3.compoundMethod.calledOnce).to.be.true;
        });

        it("compoundMethod() should skip failing compoundMethod() calls and continue to next item", () => {
            const thrower = () => {
                throw new Error("I can't perform compoundMethod()");
            };
            const item1 = new TestItem();
            const item2 = new TestItem();
            item2.compoundMethod = sinon.spy(thrower);
            const item3 = new TestItem();
            const actual = new array.CompoundItem(TestItem, "compoundMethod", item1, item2, item3);

            expect(item1.compoundMethod.calledOnce).to.be.false;
            expect(item2.compoundMethod.calledOnce).to.be.false;
            expect(item3.compoundMethod.calledOnce).to.be.false;

            actual.compoundMethod({});

            expect(item1.compoundMethod.calledOnce).to.be.true;
            expect(item2.compoundMethod.calledOnce).to.be.true;
            expect(item3.compoundMethod.calledOnce).to.be.true;
        });

        it("compoundMethod() should pass on proper parameters", () => {
            const item = new TestItem();
            item.compoundMethod = function compoundMethod(param1, param2) {
                expect(param1).to.equal(1);
                expect(param2).to.equal(2);
            };

            const actual = new array.CompoundItem(TestItem, "compoundMethod", item);

            actual.compoundMethod(1, 2);
        });

        it("add() should add item to called items", () => {
            const item1 = new TestItem();
            const item2 = new TestItem();
            const item3 = new TestItem();
            const item4 = new TestItem();
            const actual = new array.CompoundItem(TestItem, "compoundMethod", item1, item2, item3);

            expect(item1.compoundMethod.calledOnce).to.be.false;
            expect(item2.compoundMethod.calledOnce).to.be.false;
            expect(item3.compoundMethod.calledOnce).to.be.false;
            expect(item4.compoundMethod.calledOnce).to.be.false;

            actual.add(item4);
            actual.compoundMethod({});

            expect(item1.compoundMethod.calledOnce).to.be.true;
            expect(item2.compoundMethod.calledOnce).to.be.true;
            expect(item3.compoundMethod.calledOnce).to.be.true;
            expect(item4.compoundMethod.calledOnce).to.be.true;
        });

        it("add() should fail if parameter isn't an item", () => {
            const item1 = new TestItem();
            const item2 = new TestItem();
            const item3 = new TestItem();
            const actual = new array.CompoundItem(TestItem, "compoundMethod", item1, item2, item3);
            const notAnItem = {};
            const shouldThrow = () => actual.add(notAnItem);
            expect(shouldThrow).to.throw();
        });

        it("remove() should fail if parameter isn't a tagger", () => {
            const item1 = new TestItem();
            const item2 = new TestItem();
            const item3 = new TestItem();
            const actual = new array.CompoundItem(TestItem, "compoundMethod", item1, item2, item3);
            const notAnItem = {};
            const shouldThrow = () => actual.remove(notAnItem);
            expect(shouldThrow).to.throw();
        });

        it("remove() should remove initial tagger from called taggers", () => {
            const item1 = new TestItem();
            const item2 = new TestItem();
            const item3 = new TestItem();
            const actual = new array.CompoundItem(TestItem, "compoundMethod", item1, item2, item3);

            expect(item1.compoundMethod.calledOnce).to.be.false;
            expect(item2.compoundMethod.calledOnce).to.be.false;
            expect(item3.compoundMethod.calledOnce).to.be.false;

            actual.remove(item2);
            actual.compoundMethod({});

            expect(item1.compoundMethod.calledOnce).to.be.true;
            expect(item2.compoundMethod.calledOnce).to.be.false;
            expect(item3.compoundMethod.calledOnce).to.be.true;
        });

        it("remove() should remove added tagger from called taggers", () => {
            const item1 = new TestItem();
            const item2 = new TestItem();
            const item3 = new TestItem();
            const item4 = new TestItem();
            const actual = new array.CompoundItem(TestItem, "compoundMethod", item1, item2, item3);

            expect(item1.compoundMethod.calledOnce).to.be.false;
            expect(item2.compoundMethod.calledOnce).to.be.false;
            expect(item3.compoundMethod.calledOnce).to.be.false;
            expect(item4.compoundMethod.calledOnce).to.be.false;

            actual.add(item4);
            actual.remove(item4);
            actual.compoundMethod();

            expect(item1.compoundMethod.calledOnce).to.be.true;
            expect(item2.compoundMethod.calledOnce).to.be.true;
            expect(item3.compoundMethod.calledOnce).to.be.true;
            expect(item4.compoundMethod.calledOnce).to.be.false;
        });
    });
});