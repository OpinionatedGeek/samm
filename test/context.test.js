"use strict";

const {
    expect
} = require("chai");
const ctx = require("../lib/context.js");
const log = require("../lib/log.js");

describe("Context", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("parseRetries()", () => {
        it("parseRetries() should throw with invalid parameter", () => {
            const shouldThrow = () => ctx.parseRetries({});
            expect(shouldThrow).to.throw();
        });

        it("parseRetries() should return empty array for empty string", () => {
            const actual = ctx.parseRetries("");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(0);
        });

        it("parseRetries() should return an array of a single value for a single number parameter", () => {
            const actual = ctx.parseRetries("7");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(1)
                .that.has.members([7]);
        });

        it("parseRetries() should return an array of values for comma-separated number parameters", () => {
            const actual = ctx.parseRetries("7,8,9");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(3)
                .that.has.members([7, 8, 9]);
        });

        it("parseRetries() should return an array of values for spaced comma-separated number parameters", () => {
            const actual = ctx.parseRetries(" 7 , 8 , 9 ");
            expect(actual).to.be.an("array")
                .to.have.lengthOf(3)
                .that.has.members([7, 8, 9]);
        });
    });

    describe("Building Context()", () => {
        it("new Context() should provide a Context with defaults", () => {
            const actual = new ctx.Context();
            expect(actual.confirmations).to.equal(6);
            expect(actual.limit).to.equal(100);
            expect(actual.orderType).to.equal("limit");
        });

        it("buildContext() with no parameter should throw", () => {
            const shouldThrow = () => ctx.buildContext();
            expect(shouldThrow).to.throw();
        });

        it("buildContext() with no array strings should provide a Context with defaults", () => {
            const actual = ctx.buildContext([]);
            expect(actual.confirmations).to.equal(6);
            expect(actual.limit).to.equal(100);
            expect(actual.orderType).to.equal("limit");
        });

        it("buildContext() with arg string array should ignore first two elements", () => {
            const actual = ctx.buildContext([
                "ignored1",
                "ignored2"
            ]);
            expect(actual.confirmations).to.equal(6);
            expect(actual.limit).to.equal(100);
            expect(actual.orderType).to.equal("limit");
            expect(actual.ignored1).to.be.undefined;
            expect(actual.ignored2).to.be.undefined;
        });

        it("buildContext() with arg string array should ignore first two elements even if they're valid", () => {
            const actual = ctx.buildContext([
                "--confirmations",
                "7"
            ]);
            expect(actual.confirmations).to.equal(6);
            expect(actual.limit).to.equal(100);
            expect(actual.orderType).to.equal("limit");
        });

        it("buildContext() with arg string array should override defaults", () => {
            const actual = ctx.buildContext([
                "ignored1",
                "ignored2",
                "--confirmations",
                "7",
                "--limit",
                "50"
            ]);
            expect(actual.confirmations).to.equal(7);
            expect(actual.limit).to.equal(50);
            expect(actual.orderType).to.equal("limit");
        });

        it("buildContext() with command-line parameters and program defaults should override defaults", () => {
            const actual = ctx.buildContext([
                "ignored1",
                "ignored2",
                "--confirmations",
                "7",
                "--limit",
                "50"
            ], {
                confirmations: 8,
                limit: 25
            });
            expect(actual.confirmations).to.equal(7);
            expect(actual.limit).to.equal(50);
            expect(actual.orderType).to.equal("limit");
        });

        it("buildContext() with command-line parameters and program defaults should override defaults", () => {
            const actual = ctx.buildContext([
                "ignored1",
                "ignored2",
                "--confirmations",
                "7"
            ], {
                confirmations: 8,
                limit: 25
            });

            // This is from the command-line, overriding the default of 6
            expect(actual.confirmations).to.equal(7);

            // This is from the program defaults, overriding the default of 100
            expect(actual.limit).to.equal(25);

            // This is the default
            expect(actual.orderType).to.equal("limit");
        });

        it("buildContext() specifies default commitment", () => {
            const actual = ctx.buildContext([
                "ignored1",
                "ignored2"
            ]);
            expect(actual.commitment).to.equal("single");
        });

        it("buildContext() fails with invalid commitment", () => {
            const shouldThrow = () => ctx.buildContext([
                "ignored1",
                "ignored2",
                "--commitment",
                "xyz"
            ]);
            expect(shouldThrow).to.throw();
        });

        it("buildContext() succeeds with valid commitment", () => {
            const actual = ctx.buildContext([
                "ignored1",
                "ignored2",
                "--commitment",
                "recent"
            ]);
            expect(actual.commitment).to.equal("recent");
        });

        it("loadContext() creates Connection with default commitment", async () => {
            const actual = await ctx.loadContext([]);
            expect(actual.connection.commitment).to.equal("single");
        });

        it("loadContext() creates Connection with overridden commitment", async () => {
            const actual = await ctx.loadContext([
                "ignored1",
                "ignored2",
                "--commitment",
                "recent"
            ]);
            expect(actual.connection.commitment).to.equal("recent");
        });

        it("loadContext() fails with invalid commitment", async () => {
            let thrown = null;
            try {
                await ctx.loadContext([
                    "ignored1",
                    "ignored2",
                    "--commitment",
                    "xyz"
                ]);
                thrown = false;
            } catch {
                thrown = true;
            }

            expect(thrown).to.be.true;
            expect(thrown).not.to.be.null;
        });
    });
});