# 💉 Serum Automated Market Maker

## 📜 Additional Token Definitions

SAMM loads token definitions from [`serum-js`’s defined list of token](https://github.com/project-serum/serum-js/blob/master/src/token-mints.json). That covers most situations.

But one of the best things about decentralized exchanges is that they are permissionless. You can create your own tokens, set up your own markets, and you don’t need `serum-js`’s (or anyone else’s) permission to do it.


## 📖 Telling SAMM About A Token

If `serum-js` is out of date, or if you have created a token and want SAMM to work with it, you need to tell SAMM:
* The token’s symbol, and
* The token’s mint address

You combine these into a single string, separated by a colon (the mint address should be base58-encoded). So if you want to add a token with the symbol _MNB_ and it has a mint address of _AHJ8b36o3D2XA9GVr6C7gyERiF2D796oLKqxSFXX9Bqx_, you would combine them to create a string `"MNB:AHJ8b36o3D2XA9GVr6C7gyERiF2D796oLKqxSFXX9Bqx"`.

Then you just pass that string using the parameter `--additionalToken` to whatever SAMM command you’re running, like:
```
$ /var/sammd/bin/samm orders --market MNB/USDC --additionalToken "MNB:AHJ8b36o3D2XA9GVr6C7gyERiF2D796oLKqxSFXX9Bqx"
```

That should be all you need to do to make SAMM aware of the MNB token, but you will need to pass it as a parameter every time.


## 📖 Telling SAMM About Multiple Tokens

The process for telling SAMM about multiple tokens is the same as for a single token. All SAMM commands can take multiple `--additionalToken` parameters, so you just specify that parameter as many times as you need. For example, to add the token _MNB_ with mint address _AHJ8b36o3D2XA9GVr6C7gyERiF2D796oLKqxSFXX9Bqx_ and the token _LKJ_ with mint address _8N3Z8czoU9oZvt95iAVPysBX1cJXF6wHSeHA52nHh8wu_, you would use:
```
$ /var/sammd/bin/samm orders --market MNB/USDC --additionalToken "MNB:AHJ8b36o3D2XA9GVr6C7gyERiF2D796oLKqxSFXX9Bqx" --additionalToken "LKJ:8N3Z8czoU9oZvt95iAVPysBX1cJXF6wHSeHA52nHh8wu"
```
You can specify `--additionalToken` as many times as you need. Again, you will need to pass those parameters every time you want SAMM to be aware of those tokens.


## 📖 Telling SAMM About Multiple Tokens Using A File

It’s possible to add tokens to a file and have SAMM load that, instead of having to always specify tokens on the command line.

Depending on your runtime environment, that may be a better option for you. Unfortunately docker makes this a bit harder.

The file itself should be in the same format as used by `serum-js`. For example, to add the two tokens used above, the file should look like:
```
[
    {
        "address": "AHJ8b36o3D2XA9GVr6C7gyERiF2D796oLKqxSFXX9Bqx",
        "name": "MNB"
    },
    {
        "address": "8N3Z8czoU9oZvt95iAVPysBX1cJXF6wHSeHA52nHh8wu",
        "name": "LKJ"
    }
]
```
It should be an array of objects, and each object should have a `name` and an `address`, where the name is the token symbol and the address is the token mint address.

The file should be called `tokens.json` and it should be in the current directory. Alternatively, you can call the file whatever you like, place it wherever you like, and pass the `--additionalTokenFile` parameter with the full path to the file, e.g. `--additionalTokenFile /var/sammd/additionalTokens.json`.

Docker causes problems here because the additional file needs to be mapped into SAMM’s execution context inside docker. If you use the `sammd` scripts from the [QuickStart](QuickStart.md) you’ll need to update them with the additional line `-v /var/sammd/tokens.json:/home/node/app/tokens.json \\`, for example:
```
$ cat << EOF > /var/sammd/bin/sammd
#! /usr/bin/env bash
docker run --rm -d --name sammd \\
    -v /var/sammd/wallet.json:/home/node/app/wallet.json \\
    -v /var/sammd/tokens.json:/home/node/app/tokens.json \\
    -v /var/sammd/auditlogs:/home/node/app/auditlogs \\
    opinionatedgeek/samm:v0.1 \\
    \$@
docker logs --follow sammd
EOF
```
And:
```
$ cat << EOF > /var/sammd/bin/samm
#! /usr/bin/env bash
docker run --rm -it --name=samm \\
    -v /var/sammd/wallet.json:/home/node/app/wallet.json \\
    -v /var/sammd/tokens.json:/home/node/app/tokens.json \\
    -v /var/sammd/auditlogs:/home/node/app/auditlogs \\
    opinionatedgeek/samm:v0.1 \\
    \$@
EOF
```


## 📖 Using Combinations Of The Above

The load order may be important if you’re using any of the above approaches, so to be clear, token definitions are loaded in the following order:

1. Token definitions are loaded from serum-js.
2. Token definitions are then loaded from any custom token JSON file. If a symbol or address clashes, _the existing symbol or address is overwritten by the new value loaded from the file_.
3. Token definitions are then loaded from the command line. If a symbol or address clashes, _the existing symbol or address is overwritten by the new value loaded from the command line_.

Or put another way, token definitions specified on the command line takes priority over token definitions specified in a tokens.json file, which in turn takes priority over token definitions from serum.js.