"use strict";

const layouts = require("../lib/layouts.js");
const solana = require("@solana/web3.js");
const retry = require("./retry.js");

function createConnection(context) {
    const connection = new solana.Connection(context.host, context.commitment);
    const retrier = new retry.Retrier(context.retries);

    connection.isConnection = true;
    connection.confirmations = context.confirmations;
    connection._waitFor = async function _waitFor(signature) {
        const wait = this.confirmations;
        const value = await this.confirmTransaction(signature, wait);

        if (!value) {
            throw new Error("Server response was empty.");
        }

        if (value.err) {
            throw new Error(JSON.stringify(value.err));
        }

        return value;
    };

    connection.waitFor = function waitFor(signature) {
        return retrier.withRetries("Connection.waitFor()",
            () => this._waitFor(signature));
    };

    connection.sendTransactionAndWaitForConfirmation =
        function sendTransactionAndWaitForConfirmation(transaction, signers) {
            return retrier.withRetries("Connection.sendTransactionAndWaitForConfirmation()", async() => {
                const signature = await this.sendTransaction(transaction, signers);
                return this.waitFor(signature);
            });
        };

    const boundGetTokenAccountsByOwner = connection.getTokenAccountsByOwner.bind(connection);
    connection.getTokenAccountsByOwner = async function getTokenAccountsByOwner(ownerAddress, filter) {
        const intermediate = await retrier.withRetries("Connection.getTokenAccountsByOwner()",
            () => boundGetTokenAccountsByOwner(ownerAddress, filter));
        return layouts.decodeAccountResponse(intermediate);
    };

    return connection;
}

module.exports = {
    createConnection
};