"use strict";

const assert = require("./assert.js");
const Big = require("big.js");
const enums = require("./enums.js");
const numbers = require("./numbers.js");
const solana = require("@solana/web3.js");
const tags = require("./tags.js");
const token = require("./token.js");

const OrderSide = enums.createEnum(
    "OrderSide",
    [
        new enums.EnumValue("buy"),
        new enums.EnumValue("sell")
    ]
);

const OrderTypeUnspecified = new enums.EnumValue("unspecified");
const OrderType = enums.createEnum(
    "OrderType",
    [
        new enums.EnumValue("limit"),
        new enums.EnumValue("ioc"),
        new enums.EnumValue("postOnly")
    ],
    OrderTypeUnspecified
);

class Order {
    static fromSerum(serumOrder) {
        function bigOrNull(value) {
            return value || value === 0 ?
                new Big(value) :
                null;
        }
        const side = OrderSide.fromName(serumOrder.side);
        const type = OrderType.fromName(serumOrder.orderType);
        const price = new Big(serumOrder.price);
        const size = new Big(serumOrder.size);
        const orderId = serumOrder.orderId ?
            numbers.createBigFromBNAndDecimals(serumOrder.orderId, 0) :
            serumOrder.orderId;
        const clientId = serumOrder.clientId ?
            numbers.createBigFromBNAndDecimals(serumOrder.clientId, 0) :
            numbers.Zero;
        const openOrdersSlot = bigOrNull(serumOrder.openOrdersSlot);
        const feeTier = bigOrNull(serumOrder.feeTier);
        const priceLots = bigOrNull(serumOrder.priceLots);
        const sizeLots = bigOrNull(serumOrder.sizeLots);

        // This seems dodgy. We usually want (and get) a public key and a kind of 'account' object
        // that isn't really an Account object. But here we just get a public key. We can produce
        // an object that is acceptable to our code, but is it right as far as Serum is concerned?
        const payer = serumOrder.payer ?
            new token.TokenAccount(serumOrder.payer, {
                executable: false,
                owner: serumOrder.payer
            }) :
            serumOrder.payer;

        return new Order(
            side,
            serumOrder.owner,
            payer,
            type,
            price,
            size,
            orderId,
            clientId,
            serumOrder.openOrdersAddress,
            openOrdersSlot,
            feeTier,
            priceLots,
            sizeLots
        );
    }

    static fromSerumArray(untypeds) {
        return untypeds.map((untyped) => Order.fromSerum(untyped));
    }

    static cloneWithPrice(toClone, price) {
        assert.parameterType("Order.cloneWithPrice()", "toClone", toClone, Order);
        assert.parameterType("Order.cloneWithPrice()", "price", price, Big);

        return new Order(
            toClone.side,
            toClone.owner,
            toClone.payer,
            toClone.type,
            price,
            toClone.size,
            toClone.id,
            toClone.clientId,
            toClone.openOrdersAddress,
            toClone.openOrdersSlot,
            toClone.feeTier,
            toClone.priceLots,
            toClone.sizeLots
        );
    }

    // eslint-disable-next-line max-params, max-statements
    constructor(
        side,
        owner,
        payer,
        type,
        price,
        size,
        orderId,
        clientId,
        openOrdersAddress,
        openOrdersSlot,
        feeTier,
        priceLots,
        sizeLots
    ) {
        assert.parameterTypeIsEnum("Order()", "side", side, OrderSide);
        assert.parameterTypeOrUndefined("Order()", "owner", owner, solana.Account);
        assert.parameterTypeOrUndefined("Order()", "payer", payer, token.TokenAccount);
        assert.parameterTypeIsEnum("Order()", "type", type, OrderType);
        assert.parameterType("Order()", "price", price, Big);
        assert.parameterType("Order()", "size", size, Big);
        assert.parameterTypeOrUndefined("Order()", "orderId", orderId, Big);
        assert.parameterTypeOrUndefined("Order()", "clientId", clientId, Big);
        assert.parameterTypeOrUndefined("Order()", "openOrdersAddress", openOrdersAddress, solana.PublicKey);
        assert.parameterTypeOrUndefined("Order()", "openOrdersSlot", openOrdersSlot, Big);
        assert.parameterTypeOrUndefined("Order()", "feeTier", feeTier, Big);
        assert.parameterTypeOrUndefined("Order()", "priceLots", priceLots, Big);
        assert.parameterTypeOrUndefined("Order()", "sizeLots", sizeLots, Big);

        this.isOrder = true;

        this.side = side;
        this.owner = owner;
        this.payer = payer;
        this.type = type;
        this.price = price;
        this.size = size;
        this.id = orderId;
        this.clientId = clientId;
        this.openOrdersAddress = openOrdersAddress;
        this.openOrdersSlot = openOrdersSlot;
        this.feeTier = feeTier;
        this.priceLots = priceLots;
        this.sizeLots = sizeLots;

        tags.ensureTags(this);

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    toSerumOrder() {
        function toIntOrUndefined(value) {
            return value ?
                value.toFixed(0) :
                value;
        }

        const order = {
            side: this.side.name,
            owner: this.owner,
            price: Number(this.price),
            size: Number(this.size),
            openOrdersAddress: this.openOrdersAddress
        };

        if (this.type && this.type !== OrderTypeUnspecified) {
            order.orderType = this.type.name;
        }

        if (this.clientId) {
            order.clientId = numbers.createBNFromBigAndDecimals(this.clientId);
        }

        if (this.id) {
            order.orderId = numbers.createBNFromBigAndDecimals(this.id);
        }

        if (this.payer) {
            order.payer = this.payer.publicKey;
        }

        order.openOrdersSlot = toIntOrUndefined(this.openOrdersSlot);
        order.feeTier = toIntOrUndefined(this.feeTier);
        order.priceLots = toIntOrUndefined(this.priceLots);
        order.sizeLots = toIntOrUndefined(this.sizeLots);

        return order;
    }

    toString() {
        return `« Order ${this.side.name} ${this.size.toString()} at price ${this.price.toString()} [${this.clientId}] »`;
    }
}

module.exports = {
    OrderSide,
    OrderType,
    Order
};