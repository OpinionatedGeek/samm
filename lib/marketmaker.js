"use strict";

const assert = require("../lib/assert.js");
const balances = require("../lib/balances.js");
const fillreporter = require("../lib/fillreporter.js");
const formatter = require("../lib/formatter.js");
const log = require("../lib/log.js");
const orderbuilder = require("../lib/orderbuilder.js");
const ordercanceller = require("../lib/ordercanceller.js");
const orderfetcher = require("../lib/orderfetcher.js");
const orderplacer = require("../lib/orderplacer.js");
const orderwaiter = require("../lib/orderwaiter.js");
const ordertracker = require("../lib/ordertracker.js");
const settler = require("../lib/settler.js");

class MarketMaker {
    constructor(
        balanceFetcher,
        balanceSettler,
        fillReporter,
        orderBuilder,
        orderFetcher,
        orderPlacer,
        orderTracker,
        orderCanceller,
        orderWaiter
    ) {
        assert.parameterType("MarketMaker()", "balanceFetcher", balanceFetcher, balances.BalanceFetcher);
        assert.parameterType("MarketMaker()", "balanceSettler", balanceSettler, settler.Settler);
        assert.parameterType("MarketMaker()", "fillReporter", fillReporter, fillreporter.FillReporter);
        assert.parameterType("MarketMaker()", "orderBuilder", orderBuilder, orderbuilder.OrderBuilder);
        assert.parameterType("MarketMaker()", "orderFetcher", orderFetcher, orderfetcher.OrderFetcher);
        assert.parameterType("MarketMaker()", "orderPlacer", orderPlacer, orderplacer.OrderPlacer);
        assert.parameterType("MarketMaker()", "orderTracker", orderTracker, ordertracker.OrderTracker);
        assert.parameterType("MarketMaker()", "orderCanceller", orderCanceller, ordercanceller.OrderCanceller);
        assert.parameterType("MarketMaker()", "orderWaiter", orderWaiter, orderwaiter.OrderWaiter);

        this.isMarketMaker = true;

        this.balanceFetcher = balanceFetcher;
        this.balanceSettler = balanceSettler;
        this.fillReporter = fillReporter;
        this.orderBuilder = orderBuilder;
        this.orderFetcher = orderFetcher;
        this.orderPlacer = orderPlacer;
        this.orderTracker = orderTracker;
        this.orderCanceller = orderCanceller;
        this.orderWaiter = orderWaiter;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    async processTick() {
        try {
            const fillPromise = this.fillReporter.reportOnFills();

            // Ask for existing orders
            let existing = null;
            const existingPromise = this.orderFetcher.fetchExistingOrders().then((fetched) => {
                existing = fetched;
            });

            await Promise.all([fillPromise, existingPromise]);

            if (existing.length === 0 || !this.orderTracker.allIdsTracked(existing.map((order) => order.clientId))) {
                // Cancel remaining orders
                await this.orderCanceller.cancelOrders(existing);

                // Settle anything we need to settle, every time - tip from SamS.
                await this.balanceSettler.settle();

                // Reset our tracker
                this.orderTracker.clearTrackedOrders();

                // Ask for all balances
                const currentBalances = await this.balanceFetcher.fetchBalance();
                log.print(`Balances: ${currentBalances}`);

                // Create fresh orders
                const freshOrders = await this.orderBuilder.buildOrders(currentBalances);
                log.print("Orders to place:", freshOrders.map(
                    (order) => formatter.orderDetails(order)
                ).join(""));

                // Add the orders to our tracking
                this.orderTracker.trackOrders(freshOrders);

                // Actually place the orders on the order book
                await this.orderPlacer.placeOrders(freshOrders);

                // Now wait until the orders appear on the OrderBook. This should be very quick,
                // but sometimes it isn't.
                await this.orderWaiter.waitForOrders(freshOrders);
            }
        } catch (ex) {
            log.error(ex);
        }
    }

    async cleanup() {
        log.print("MarketMaker.cleanup() Cleaning up existing orders");

        try {
            const existing = await this.orderFetcher.fetchExistingOrders();
            if (existing.length === 0) {
                log.print(`MarketMaker.cleanup() Cancelling ${existing.length} existing orders...`);
                // Cancel remaining orders
                await this.orderCanceller.cancelOrders(existing);

                log.print("MarketMaker.cleanup() Done. Settling...");
                // Settle anything we need to settle, every time - tip from SamS.
                await this.balanceSettler.settle();
            }
        } catch (ex) {
            log.error(ex);
        }

        log.print("MarketMaker.cleanup() Cleanup complete.");
    }
}

module.exports = {
    MarketMaker
};