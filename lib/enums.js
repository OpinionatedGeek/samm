"use strict";

const assert = require("./assert.js");

class EnumValue {
    constructor(name) {
        assert.parameterTypeIsString("EnumValue()", "name", name);

        this.name = name;

        this.isEnumValue = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    get value() {
        return this.name.toUpperCase();
    }

    toString() {
        return `« ${this.name.toUpperCase()} »`;
    }
}

class Enum {
    constructor(typeName, allValues, unspecified) {
        assert.parameterTypeIsString("Enum()", "typeName", typeName);
        assert.parameterTypeIsArrayOfType("Enum()", "allValues", allValues, EnumValue);

        this.typeName = typeName;
        this.allValues = allValues;
        this.unspecified = unspecified;

        this.isEnum = true;

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    fromName(name) {
        const nameToFind = name || "";
        assert.parameterTypeIsString("Enum()", "name", nameToFind);

        return this.allValues.find((enumValue) => enumValue.value === nameToFind.toUpperCase()) ||
            this.unspecified ||
            (() => {
                throw new Error(`Unknown enum value: ${name} in enum type ${this.typeName}`);
            })();
    }

    hasValue(value) {
        return Boolean(this.allValues.find((enumValue) => enumValue === value)) || value === this.unspecified;
    }

    toString() {
        const allNames = this.allValues.map((value) => value.value);
        return `« '${this.typeName}' enum with values [${allNames.join(", ")}] »`;
    }
}

function createEnum(typeName, allValues, unspecified) {
    const inner = new Enum(typeName, allValues, unspecified);
    return new Proxy(inner, {
        get(target, name, receiver) {
            if (!Reflect.has(target, name)) {
                return target.fromName(name);
            }
            return Reflect.get(target, name, receiver);
        }
    });
}

module.exports = {
    EnumValue,
    Enum,
    createEnum
};