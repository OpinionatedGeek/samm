"use strict";

const accounttracker = require("../lib/accounttracker.js");
const assert = require("./assert.js");
const Big = require("big.js");
const constantproduct = require("../lib/constantproduct.js");
const log = require("../lib/log.js");
const order = require("../lib/order.js");
const ordertracker = require("../lib/ordertracker.js");
const serum = require("@project-serum/serum");
const solana = require("@solana/web3.js");
const spreads = require("./spreads.js");

class OrderBuilder {
    constructor() {
        assert.notConstructingAbstractType(OrderBuilder, new.target);

        this.isOrderBuilder = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    buildOrders() {
        assert.throwOnAbstractCall(OrderBuilder, this.buildOrders);
    }
}

class ConstantProductOrderBuilder extends OrderBuilder {
    // I'd rather keep this constructor straightforward than mess it up to please the linter.
    // eslint-disable-next-line max-statements
    constructor(market, accountTracker, orderTracker, owner, positionSizeProportion, fee) {
        super();

        assert.parameterType("ConstantProductOrderBuilder()", "market", market, serum.Market);
        // eslint-disable-next-line max-len
        assert.parameterType("ConstantProductOrderBuilder()", "accountTracker", accountTracker, accounttracker.AccountTracker);
        assert.parameterType("ConstantProductOrderBuilder()", "orderTracker", orderTracker, ordertracker.OrderTracker);
        assert.parameterType("ConstantProductOrderBuilder()", "owner", owner, solana.Account);
        assert.parameterType("ConstantProductOrderBuilder()", "positionSizeProportion", positionSizeProportion, Big);
        assert.parameterType("ConstantProductOrderBuilder()", "fee", fee, Big);

        this.market = market;
        this.accountTracker = accountTracker;
        this.orderTracker = orderTracker;
        this.owner = owner;
        this.positionSizeProportion = positionSizeProportion;
        this.fee = fee;

        if (new.target === ConstantProductOrderBuilder) {
            Object.freeze(this);
        }
    }

    buildOrders(balances) {
        const orders = [];
        const calculator = new constantproduct.ConstantProductCalculator(
            balances.total.base,
            balances.total.quote,
            this.fee
        );

        const buy = calculator.costForBuyingBaseProportion(this.positionSizeProportion);
        if (buy) {
            const buyOrder = new order.Order(
                order.OrderSide.BUY,
                this.owner,
                this.accountTracker.quoteTokenAccounts[0],
                order.OrderType.POSTONLY,
                buy.price,
                buy.size,
                null,
                this.orderTracker.nextClientId()
            );

            orders.push(buyOrder);
        }

        const sell = calculator.costForSellingBaseProportion(this.positionSizeProportion);
        if (sell) {
            const sellOrder = new order.Order(
                order.OrderSide.SELL,
                this.owner,
                this.accountTracker.baseTokenAccounts[0],
                order.OrderType.POSTONLY,
                sell.price,
                sell.size,
                null,
                this.orderTracker.nextClientId()
            );

            orders.push(sellOrder);
        }

        return orders;
    }
}

class SpreadAdjustingOrderBuilder extends OrderBuilder {
    constructor(orderBuilder, spreadFetcher, tickSize) {
        super();

        assert.parameterType("SpreadAdjustingOrderBuilder()", "orderBuilder", orderBuilder, OrderBuilder);
        assert.parameterType("SpreadAdjustingOrderBuilder()", "spreadFetcher", spreadFetcher, spreads.SpreadFetcher);
        assert.parameterType("SpreadAdjustingOrderBuilder()", "tickSize", tickSize, Big);

        this.innerOrderBuilder = orderBuilder;
        this.spreadFetcher = spreadFetcher;
        this.tickSize = tickSize;

        if (new.target === SpreadAdjustingOrderBuilder) {
            Object.freeze(this);
        }
    }

    async buildOrders(balances) {
        const desired = await this.innerOrderBuilder.buildOrders(balances);
        const spread = await this.spreadFetcher.fetchSpread();
        log.print(`Spread: ${spread}`);

        const orders = [];
        for (const immutableOrder of desired) {
            let adjustedOrder = immutableOrder;
            const originalOrderPrice = order.price;
            switch (immutableOrder.side) {
                case order.OrderSide.BUY:
                // If our buy price is outside the spread (higher than the best ask) we move our buy
                // price inside the spread by putting it one tick higher than the current best bid.
                    if (adjustedOrder.price.gt(spread.bestAsk)) {
                        adjustedOrder = order.Order.cloneWithPrice(adjustedOrder, spread.bestBid.plus(this.tickSize));
                        log.warn(`Adjusted buy price from ${originalOrderPrice} to ${adjustedOrder.price} to be within spread ${spread}`);
                    } else {
                        log.print(`Buy price is ${adjustedOrder.price} - lower than best ask in spread ${spread}`);
                    }
                    break;

                case order.OrderSide.SELL:
                // If our sell price is outside the spread (lower than the best bid) we move our sell
                // price inside the spread by putting it one tick lower than the current best ask.
                    if (adjustedOrder.price.lt(spread.bestBid)) {
                        adjustedOrder = order.Order.cloneWithPrice(adjustedOrder, spread.bestAsk.minus(this.tickSize));
                        log.warn(`Adjusted sell price from ${originalOrderPrice} to ${adjustedOrder.price} to be within spread ${spread}`);
                    } else {
                        log.print(`Sell price is ${adjustedOrder.price} - higher than best bid in spread ${spread}`);
                    }
                    break;

                default:
                    throw new Error(`SpreadAdjustingOrderBuilder.buildOrders(): Unknown order side when building order: ${adjustedOrder.side}`);
            }

            orders.push(adjustedOrder);
        }

        return orders;
    }
}

class PositionSizeFilteringOrderBuilder extends OrderBuilder {
    constructor(orderBuilder) {
        super();

        // eslint-disable-next-line max-len
        assert.parameterType("PositionSizeFilteringOrderBuilder()", "orderBuilder", orderBuilder, OrderBuilder);

        this.innerOrderBuilder = orderBuilder;

        if (new.target === PositionSizeFilteringOrderBuilder) {
            Object.freeze(this);
        }
    }

    async buildOrders(balances) {
        const desired = await this.innerOrderBuilder.buildOrders(balances);

        const orders = [];
        for (const immutableOrder of desired) {
            switch (immutableOrder.side) {
                case order.OrderSide.BUY:
                    if (balances.actualTotal.quote.value.gte(immutableOrder.size * immutableOrder.price)) {
                        orders.push(immutableOrder);
                    } else {
                        log.warn(`Removing buy order because balance of ${balances.actualTotal.quote} is less than the cost of ${immutableOrder.size * immutableOrder.price}`);
                    }
                    break;

                case order.OrderSide.SELL:
                    if (balances.actualTotal.base.value.gte(immutableOrder.size)) {
                        orders.push(immutableOrder);
                    } else {
                        log.warn(`Removing sell order because balance of ${balances.actualTotal.base} is less than the order size of ${immutableOrder.size}`);
                    }
                    break;

                default:
                    throw new Error(`PositionSizeFilteringOrderBuilder.buildOrders(): Unknown order side when building order: ${immutableOrder.side}`);
            }
        }

        return orders;
    }
}

function createOrderBuilder(market, accountTracker, orderTracker, owner, positionSizeProportion, fee) {
    const orderBuilder = new ConstantProductOrderBuilder(
        market,
        accountTracker,
        orderTracker,
        owner,
        positionSizeProportion,
        fee
    );
    const spreadAdjuster = new SpreadAdjustingOrderBuilder(
        orderBuilder,
        market.spreadFetcher(),
        new Big(market.tickSize)
    );
    const positionSizeFilter = new PositionSizeFilteringOrderBuilder(spreadAdjuster);

    return positionSizeFilter;
}

module.exports = {
    OrderBuilder,
    ConstantProductOrderBuilder,
    SpreadAdjustingOrderBuilder,
    PositionSizeFilteringOrderBuilder,
    createOrderBuilder
};