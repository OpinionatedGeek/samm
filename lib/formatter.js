"use strict";

const array = require("../lib/array.js");
const chalk = require("chalk");
const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");
const order = require("./order.js");

const DESCRIPTION_WIDTH = 20;
const ORDER_SIDE_WIDTH = 10;
const ORDER_ID_WIDTH = 25;
const ORDER_ELEMENT_WIDTH = 10;
const PUBLIC_KEY_WIDTH = 44;
const SYMBOL_WIDTH = 9;
const BALANCES_WIDTH = 30;
const PORTION_WIDTH = 25;
const SUPPLY_WIDTH = 37;
const PRICE_DECIMALS = 2;
const SIZE_DECIMALS = 8;
const POOL_BALANCE_DECIMALS = 5;

const JSON_INDENTATION = 4;

function bigIntToString(big) {
    if (big) {
        return big.toFixed(0);
    }

    return "";
}

function reportUnknownProperties(obj, knownProperties) {
    let unknown = "";
    for (const index in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, index)) {
            if (!knownProperties.includes(index)) {
                unknown += `
    Unknown attribute - ${index}: ${obj[index]}`;
            }
        }
    }

    return unknown;
}

function describeFlags(eventFlags) {
    const flags = [];
    for (const index in eventFlags) {
        if (Object.prototype.hasOwnProperty.call(eventFlags, index)) {
            if (eventFlags[index]) {
                flags.push(index);
            }
        }
    }

    return flags.join(" | ");
}

function tagFormatter(tagged) {
    const formattedTags = [];
    if (tagged.tags) {
        for (const tag of tagged.tags) {
            if (tag.is("mine")) {
                array.insertAtHead(formattedTags, chalk.yellow.bold(tag));
            } else if (tag.hasPrefix("mine")) {
                formattedTags.push(chalk.yellow.bold(tag));
            } else if (tag.is("notmine")) {
                array.insertAtHead(formattedTags, chalk.blue.bold(tag));
            // } else if (tag.hasPrefix("notmine")) {
            //     formattedTags.push(chalk.blue.bold(tag));
            } else {
                formattedTags.push(tag);
            }
        }
    }

    const stringified = formattedTags.join(", ");
    return `[${stringified}]`;
}

function balanceDetailsFormatter(description, total, unsettled, tokens) {
    let {
        symbol
    } = total.token;
    if (tokens) {
        const maxWidth = Math.max(...tokens.map((tok) => tok.symbol.length));
        symbol = symbol.padStart(maxWidth);
    }

    const formattedName = chalk.white.bold(description.padEnd(DESCRIPTION_WIDTH));
    const totalRounded = total.roundQuantity(total.value);
    const formattedTotal = chalk.green(`${totalRounded} ${symbol}`.padStart(DESCRIPTION_WIDTH));
    let formattedUnsettled = "";
    if (unsettled && unsettled.value.gt(0)) {
        const unsettledRounded = unsettled.roundQuantity(unsettled.value);
        formattedUnsettled = chalk.yellow(` (of which ${unsettledRounded} ${unsettled.token.symbol} is unsettled)`);
    }

    return `${formattedName} ${formattedTotal}${formattedUnsettled}`;
}

function balanceFormatter(total, unsettled, tokens) {
    const formattedName = chalk.white(`${total.token.symbol} tokens:`.padEnd(DESCRIPTION_WIDTH));
    return balanceDetailsFormatter(formattedName, total, unsettled, tokens);
}

function balancesReportFormatter(solToken, solBalance, tokenBalances, spread) {
    const allTokens = [tokenBalances.total.base.token, tokenBalances.total.quote.token, solToken];
    const formattedSpread = chalk.yellow(`(based on spread ${spread})`);
    const lines = [];
    lines.push(balanceFormatter(solBalance, null, allTokens));
    lines.push(balanceFormatter(tokenBalances.total.base, tokenBalances.unsettled.base, allTokens));
    lines.push(balanceFormatter(tokenBalances.total.quote, tokenBalances.unsettled.quote, allTokens));

    const notionalQuoteInBase = tokenBalances.total.quote.value.div(spread.bestAsk);
    const notionalBaseTotal = tokenBalances.total.base.add(notionalQuoteInBase);
    // eslint-disable-next-line no-undefined
    const formattedBase = balanceDetailsFormatter(`Notional ${tokenBalances.total.base.token.symbol} total:`, notionalBaseTotal, undefined, allTokens);
    lines.push(`${formattedBase} ${formattedSpread}`);

    const notionalBaseInQuote = tokenBalances.total.base.value.times(spread.bestBid);
    const notionalQuoteTotal = tokenBalances.total.quote.add(notionalBaseInQuote);
    // eslint-disable-next-line no-undefined
    const formattedQuote = balanceDetailsFormatter(`Notional ${tokenBalances.total.quote.token.symbol} total:`, notionalQuoteTotal, undefined, allTokens);
    lines.push(`${formattedQuote} ${formattedSpread}`);

    return lines;
}

function orderFormatter(orderToFormat) {
    const side = `${orderToFormat.side.name.toUpperCase()} order`;
    const sideFormatted = chalk.white.bold(side.padEnd(ORDER_SIDE_WIDTH));
    const orderId = numbers.isZeroOrUndefined(orderToFormat.clientId) ?
        orderToFormat.id || "<no ID>" :
        orderToFormat.clientId;
    const id = chalk.blue.bold(orderId.toString(numbers.DECIMAL_RADIX).padStart(ORDER_ID_WIDTH));
    let priceColor = chalk.red;
    if (orderToFormat.side === order.OrderSide.BUY) {
        priceColor = chalk.green;
    }
    const price = priceColor.bold(orderToFormat.price.toFixed(PRICE_DECIMALS).padStart(ORDER_ELEMENT_WIDTH));
    const size = priceColor.bold(orderToFormat.size.toFixed(SIZE_DECIMALS).padStart(ORDER_ELEMENT_WIDTH));

    return `${sideFormatted} ${id} ${price} ${size}`;
}

function orderDetailsFormatter(orderToFormat) {
    let sideColor = chalk.red;
    if (orderToFormat.side === order.OrderSide.BUY) {
        sideColor = chalk.green;
    }

    return `
  « Order »
    side:      ${sideColor.bold(orderToFormat.side)}
    type:      ${orderToFormat.type}
    id:        ${orderToFormat.clientId.toString(numbers.DECIMAL_RADIX)}
    clientId:  ${orderToFormat.clientId.toString(numbers.DECIMAL_RADIX)}
    owner:     ${orderToFormat.owner.publicKey.toBase58()}
    payer:     ${orderToFormat.payer}
    price:     ${sideColor.bold(orderToFormat.price.toFixed(PRICE_DECIMALS))}
    size:      ${sideColor.bold(orderToFormat.size.toFixed(SIZE_DECIMALS))}
`;
}

const KNOWN_FILL_PROPERTIES = [
    "eventFlags",
    "openOrdersSlot",
    "feeTier",
    "isFill",
    "key",
    "nativeQuantityReleased",
    "nativeQuantityPaid",
    "nativeFeeOrRebate",
    "orderId",
    "openOrders",
    "clientOrderId",
    "side",
    "price",
    "feeCost",
    "size",
    "tags"
];

function fillFormatter(fill) {
    const flags = describeFlags(fill.eventFlags);
    const unknown = reportUnknownProperties(fill, KNOWN_FILL_PROPERTIES);
    const formattedTags = tagFormatter(fill);

    let sideColor = chalk.red;
    if (fill.side === order.OrderSide.BUY) {
        sideColor = chalk.green;
    }

    return `
  « Fill »
    flags: ${flags}
    orderId: ${bigIntToString(fill.orderId)}
    clientOrderId: ${bigIntToString(fill.clientOrderId)}
    openOrders: ${fill.openOrders}
    openOrdersSlot: ${fill.openOrdersSlot}
    side: ${sideColor.bold(fill.side)}
    price: ${sideColor.bold(fill.price)}
    size: ${sideColor.bold(fill.size)}
    feeCost: ${fill.feeCost}
    feeTier: ${fill.feeTier}
    nativeQuantityReleased: ${bigIntToString(fill.nativeQuantityReleased)}
    nativeQuantityPaid: ${bigIntToString(fill.nativeQuantityPaid)}
    nativeFeeOrRebate: ${bigIntToString(fill.nativeFeeOrRebate)}
    tags: ${formattedTags}${unknown}
`;
}

const KNOWN_EVENT_PROPERTIES = [
    "eventFlags",
    "openOrdersSlot",
    "feeTier",
    "isEvent",
    "key",
    "nativeQuantityReleased",
    "nativeQuantityPaid",
    "nativeFeeOrRebate",
    "orderId",
    "openOrders",
    "clientOrderId",
    "tags"
];

function eventFormatter(event) {
    const flags = describeFlags(event.eventFlags);
    const unknown = reportUnknownProperties(event, KNOWN_EVENT_PROPERTIES);
    const formattedTags = tagFormatter(event);

    return `
  « Event »
    flags: ${flags}
    orderId: ${bigIntToString(event.orderId)}
    clientOrderId: ${bigIntToString(event.clientOrderId)}
    openOrdersSlot: ${event.openOrdersSlot}
    feeTier: ${event.feeTier}
    nativeQuantityReleased: ${bigIntToString(event.nativeQuantityReleased)}
    nativeQuantityPaid: ${bigIntToString(event.nativeQuantityPaid)}
    nativeFeeOrRebate: ${bigIntToString(event.nativeFeeOrRebate)}
    openOrders: ${event.openOrders}
    tags: ${formattedTags}${unknown}
`;
}

const KNOWN_REQUEST_PROPERTIES = [
    "requestFlags",
    "openOrdersSlot",
    "feeTier",
    "isRequest",
    "key",
    "maxBaseSizeOrCancelId",
    "nativeQuoteQuantityLocked",
    "orderId",
    "openOrders",
    "clientOrderId",
    "tags"
];

function requestFormatter(request) {
    const flags = describeFlags(request.requestFlags);
    const unknown = reportUnknownProperties(request, KNOWN_REQUEST_PROPERTIES);
    const formattedTags = tagFormatter(request);

    return `
  « Request »
    flags: ${flags}
    orderId: ${bigIntToString(request.orderId)}
    clientOrderId: ${bigIntToString(request.clientOrderId)}
    openOrdersSlot: ${request.openOrdersSlot}
    feeTier: ${request.feeTier}
    nativeQuoteQuantityLocked: ${bigIntToString(request.nativeQuoteQuantityLocked)}
    openOrders: ${request.openOrders}
    tags: ${formattedTags}${unknown}
`;
}

// A raw OpenOrders object looks like this:
// OpenOrders {
//     address: PublicKey { },
//     _programId: PublicKey { },
//     accountFlags: {
//       initialized: true,
//       market: false,
//       openOrders: true,
//       requestQueue: false,
//       eventQueue: false,
//       bids: false,
//       asks: false
//     },
//     market: PublicKey { },
//     owner: PublicKey { },
//     baseTokenFree: <BN: 0>,
//     baseTokenTotal: <BN: 0>,
//     quoteTokenFree: <BN: 0>,
//     quoteTokenTotal: <BN: 0>,
//     freeSlotBits: <BN: ffffffffffffffffffffffffffffffff>,
//     isBidBits: <BN: 0>,
//     orders: [
//       ... 128 more items
//     ],
//     clientIds: [
//       ... 128 more items
//     ],
//     referrerRebatesAccrued: <BN: 0>
//   }
// ]
function openOrdersFormatter(market, openOrders) {
    const marketAddress = chalk.blue.bold(openOrders.market);
    const baseFree = market.baseSplSizeToNumber(openOrders.baseTokenFree);
    const baseTotal = market.baseSplSizeToNumber(openOrders.baseTokenTotal);
    const base = chalk.green(`${baseFree}${market.base} free (of ${baseTotal}${market.base})`);

    const quoteFree = market.quoteSplSizeToNumber(openOrders.quoteTokenFree);
    const quoteTotal = market.quoteSplSizeToNumber(openOrders.quoteTokenTotal);
    const quote = chalk.green(`${quoteFree}${market.quote} free (of ${quoteTotal}${market.quote})`);

    return `${marketAddress} ${base}, ${quote}`;
}

function reportIds(title, ids) {
    const report = [];
    for (const id of ids) {
        if (!numbers.Zero.eq(id)) {
            report.push(`    ${title} ID: ${id}`);
        }
    }

    if (report.length === 0) {
        return `    No ${title} IDs`;
    }

    return report.join("\n");
}

function openOrdersDetailsFormatter(market, openOrders) {
    const address = chalk.blue.bold(openOrders.address.toString());
    const orderIds = reportIds("Order", openOrders.orders);
    const clientIds = reportIds("Client", openOrders.clientIds);
    return `
  « OpenOrders »
    Address: ${address}
    Owner: ${openOrders.owner.toString()}
    Market: ${openOrders.market.toString()}
    Base Token: ${openOrders.baseTokenTotal.toString()} ${market.base} (${openOrders.baseTokenFree.toString()} ${market.base} free)
    Quote Token: ${openOrders.quoteTokenTotal.toString()} ${market.quote} (${openOrders.quoteTokenFree.toString()} ${market.quote} free)
${orderIds}
${clientIds}
`;
}

function spreadFormatter(spread) {
    const marketName = `${spread.baseToken.symbol}/${spread.quoteToken.symbol}`;
    const spreadFormatted = chalk.green(spread);
    const leadFormatted = chalk.white.bold(`${marketName} Spread:`.padEnd(DESCRIPTION_WIDTH));

    return `${leadFormatted} ${spreadFormatted}`;
}

function publicKeyFormatter(publicKey) {
    return `«${publicKey.toString().padEnd(PUBLIC_KEY_WIDTH)}»`;
}

function tokenSwapAccountStateFormatter(tokenSwapState) {
    const baseSymbol = tokenSwapState.baseToken.symbol.padEnd(SYMBOL_WIDTH);
    const quoteSymbol = tokenSwapState.quoteToken.symbol.padEnd(SYMBOL_WIDTH);
    const supply = tokenSwapState.supply.toString().padStart(SUPPLY_WIDTH);

    const myPercentage = numbers.toPercentageString(tokenSwapState.ownedFraction, POOL_BALANCE_DECIMALS);
    const notionalBase = tokenSwapState.ownedNotionalBase.toFixed(SIZE_DECIMALS).padStart(PORTION_WIDTH);
    const notionalQuote = tokenSwapState.ownedNotionalQuote.toFixed(SIZE_DECIMALS).padStart(PORTION_WIDTH);
    return `${tokenSwapState.name} Balance: ${tokenSwapState.ownedBalance}
    ${publicKeyFormatter(tokenSwapState.poolAddress)} Total Supply: ${supply}
    ${publicKeyFormatter(tokenSwapState.baseTokenAddress)} [${baseSymbol}] Balance: ${tokenSwapState.tokenBalances.base.toString().padStart(BALANCES_WIDTH)}
    ${publicKeyFormatter(tokenSwapState.quoteTokenAddress)} [${quoteSymbol}] Balance: ${tokenSwapState.tokenBalances.quote.toString().padStart(BALANCES_WIDTH)}

    Account [${tokenSwapState.owner.publicKey}] owns ${myPercentage} of pool:
    [${baseSymbol}] (notional ${myPercentage} of pool): ${notionalBase} ${tokenSwapState.baseToken.symbol}
    [${quoteSymbol}] (notional ${myPercentage} of pool): ${notionalQuote} ${tokenSwapState.quoteToken.symbol}
`;
}

function makeOutput(context, displayFormatter) {
    function doNothing() {
        // A simple do-nothing that can be substituted for log.print.
    }

    function ifNotEmpty(printer) {
        return (item) => {
            if (item) {
                printer(item);
            }
        };
    }

    let format = displayFormatter;
    let rawOutput = false;
    if (context.json) {
        format = (item) => `${JSON.stringify(item, null, JSON_INDENTATION)},`;
        rawOutput = true;
    } else if (context.csv) {
        format = (item) => item.toCsv();
        rawOutput = true;
    }

    const print = rawOutput ?
    // eslint-disable-next-line no-console
        (item) => ifNotEmpty(console.log)(item) :
        (item) => ifNotEmpty(log.print)(item);
    const messagePrint = rawOutput ?
        doNothing :
        (item) => log.print(item);
    return {
        formatter: format,
        printer: print,
        messagePrinter: messagePrint
    };
}

module.exports = {
    balance: balanceFormatter,
    balanceDetails: balanceDetailsFormatter,
    balancesReport: balancesReportFormatter,
    order: orderFormatter,
    orderDetails: orderDetailsFormatter,
    fill: fillFormatter,
    event: eventFormatter,
    request: requestFormatter,
    openOrders: openOrdersFormatter,
    openOrdersDetails: openOrdersDetailsFormatter,
    spread: spreadFormatter,
    tokenSwapAccountState: tokenSwapAccountStateFormatter,
    makeOutput
};