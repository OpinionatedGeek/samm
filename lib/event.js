"use strict";

const numbers = require("./numbers.js");
const tags = require("./tags.js");

// An event from Serum usually looks something like as an object:
// {
//     eventFlags: { fill: false, out: true, bid: true, maker: false },
//     openOrdersSlot: 12,
//     feeTier: 0,
//     nativeQuantityReleased: <BN: 5542baa>,
//     nativeQuantityPaid: <BN: 14d0ca8db8>,
//     nativeFeeOrRebate: <BN: 0>,
//     orderId: <BN: 99daffffffffffd1bfdc>,
//     openOrders: PublicKey {
//       _bn: <BN: f44c72e7ef3360a73e8374ce7ebfa56ee10ba6b34aa7222ce782efa971841e5>
//     },
//     clientOrderId: <BN: f3c0bc23db11061b>
// }
// And as JSON:
// {
//     "eventFlags": {
//         "fill": false,
//         "out": true,
//         "bid": true,
//         "maker": false
//     },
//     "openOrdersSlot": 12,
//     "feeTier": 0,
//     "nativeQuantityReleased": "05542baa",
//     "nativeQuantityPaid": "14d0ca8db8",
//     "nativeFeeOrRebate": "00",
//     "orderId": "99daffffffffffd1bfdc",
//     "openOrders": {
//         "_bn": "0f44c72e7ef3360a73e8374ce7ebfa56ee10ba6b34aa7222ce782efa971841e5"
//     },
//     "clientOrderId": "f3c0bc23db11061b"
// }
class Event {
    static fromSerum(untyped) {
        const clone = {
            ...untyped
        };

        clone.orderId = numbers.convertBNToBigOrUndefined(untyped.orderId);
        clone.clientOrderId = numbers.convertBNToBigOrUndefined(untyped.clientOrderId);
        clone.nativeQuantityReleased = numbers.convertBNToBigOrUndefined(untyped.nativeQuantityReleased);
        clone.nativeQuantityPaid = numbers.convertBNToBigOrUndefined(untyped.nativeQuantityPaid);
        clone.nativeFeeOrRebate = numbers.convertBNToBigOrUndefined(untyped.nativeFeeOrRebate);

        const keys = [clone.openOrders.toString(), clone.orderId.toFixed(0), clone.nativeQuantityReleased.toFixed(0)];
        clone.key = keys.join("+");

        return new Event(clone);
    }

    static fromSerumArray(untypeds) {
        return untypeds.map((untyped) => Event.fromSerum(untyped));
    }

    constructor(
        untyped
    ) {
        Object.assign(this, untyped);

        this.isEvent = true;

        tags.ensureTags(this);
        this.tags.addFlags(this.eventFlags);

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    toString() {
        return `« Event ID ${this.orderId} [${this.clientOrderId}] »`;
    }
}

module.exports = {
    Event
};