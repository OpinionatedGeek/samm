"use strict";

// Constant Product summary from ‘Formal Specification of Constant Product (x × y = k)
// Market Maker Model and Implementation’ by Yi Zhang, Xiaohong Chen, and Daejun Park,
// Runtime Verification, Inc., October 24, 2018
//
// Available in /assets
//
// Formal Overview of x × y = k Model
// ----------------------------------
// Consider a decentralized exchange [2] that trades two tokens X and Y. Let x and y be the
// number of tokens X and Y, respectively, that the exchange currently reserves. The token
// exchange price is determined by the ratio of x and y so that the product x × y is
// preserved. That is, when you sell ∆x tokens, you will get ∆y tokens such that
// x×y = (x+∆x)×(y−∆y). Thus, the price (∆x/∆y) is the function of x/y. Specifically,
// when you trade ∆x with ∆y, the exchange token reserves are updated as follows:
//
//   x′ = x+∆x = (1+α)x = (1 / (1−β))x
//   y′ = y−∆y = (1 / (1+α))y = (1−β)y
//
// where α = ∆x/x and β = ∆y/y

const assert = require("./assert.js");
const Big = require("big.js");
const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");
const token = require("./token.js");

// eslint-disable-next-line id-length
function calculateUpdatedBalances(x, y, deltaX, fee) {
    const one = numbers.One;
    const product = x.times(y);
    log.trace("calculateUpdatedBalances():", "Product is:", product.toString());

    // To calculate x′ρ and y′ρ (x′ and y′ taking into account a fee of ρ):
    //   x′ρ = x+∆x = (1+α)x = ((1+β((1/γ)−1))/(1−β))x
    //   y′ρ = y−∆y = (1/(1+αγ))y = (1−β)y
    // where α = ∆x/x, β = ∆y/y, ρ as the fee, and γ = 1 − ρ

    log.trace("calculateUpdatedBalances():", "x:", x.toString());
    log.trace("calculateUpdatedBalances():", "∆x:", deltaX.toString());

    // x′ = x+∆x = (1+α)x = ((1+β((1/γ)−1))/(1−β))x
    const xPrime = x.add(deltaX);
    log.trace("calculateUpdatedBalances():", "x′ρ:", xPrime.toString());

    // α = ∆x/x
    const alpha = deltaX.div(x);
    log.trace("calculateUpdatedBalances():", "α:", alpha.toString());

    // γ = 1 − ρ
    const gamma = one.minus(fee);
    log.trace("calculateUpdatedBalances():", "γ:", gamma.toString());

    // αγ
    const alphagamma = alpha.times(gamma);
    log.trace("calculateUpdatedBalances():", "αγ:", alphagamma.toString());

    // y′ = y−∆y = (1 / (1+αγ))y = (1−β)y
    const yPrime = one.div(one.add(alphagamma)).times(y);
    log.trace("calculateUpdatedBalances():", "y′ρ:", yPrime.toString());

    const newProduct = xPrime.times(yPrime);
    log.trace("calculateUpdatedBalances():", "Product after ∆ is:", newProduct.toString());

    return {
        // eslint-disable-next-line id-length
        x: xPrime,
        // eslint-disable-next-line id-length
        y: yPrime
    };
}

class ConstantProductCalculator {
    constructor(baseTokenValue, quoteTokenValue, fee) {
        assert.parameterType("ConstantProductCalculator()", "baseTokenValue", baseTokenValue, token.TokenValue);
        assert.parameterType("ConstantProductCalculator()", "quoteTokenValue", quoteTokenValue, token.TokenValue);
        assert.parameterType("ConstantProductCalculator()", "fee", fee, Big);

        this.isConstantProductCalculator = true;

        this.baseTokenValue = baseTokenValue;
        this.quoteTokenValue = quoteTokenValue;
        this.fee = fee;

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    costForBuyingBaseValue(deltaX) {
        const roundedDeltaX = this.baseTokenValue.roundQuantity(deltaX);
        if (numbers.Zero.eq(roundedDeltaX)) {
            log.warn(`Rounded buy size of ${deltaX} for ${this.baseTokenValue.token} is zero.`);
            return null;
        }

        const newBalances = calculateUpdatedBalances(
            this.baseTokenValue.value,
            this.quoteTokenValue.value,
            roundedDeltaX,
            this.fee
        );
        const totalCost = this.quoteTokenValue.value.minus(newBalances.y);
        const price = totalCost.div(roundedDeltaX);
        const roundedPrice = this.quoteTokenValue.roundQuantity(price);
        return {
            sizeToken: this.baseTokenValue.token,
            priceToken: this.quoteTokenValue.token,
            size: roundedDeltaX,
            totalCost,
            price: roundedPrice
        };
    }

    costForBuyingBaseProportion(proportion) {
        const deltaX = this.baseTokenValue.value.times(proportion);
        return this.costForBuyingBaseValue(deltaX);
    }

    costForSellingBaseValue(deltaX) {
        const roundedDeltaX = this.baseTokenValue.roundQuantity(deltaX);
        if (numbers.Zero.eq(roundedDeltaX)) {
            log.warn(`Rounded sell size of ${deltaX} for ${this.baseTokenValue.token} is zero.`);
            return null;
        }

        const fee = numbers.negate(this.fee);
        const newBalances = calculateUpdatedBalances(
            this.baseTokenValue.value,
            this.quoteTokenValue.value,
            numbers.negate(roundedDeltaX),
            fee
        );

        // totalCost comes back negative when selling.
        const totalCost = numbers.negate(this.quoteTokenValue.value.minus(newBalances.y));
        const price = totalCost.div(roundedDeltaX);
        const roundedPrice = this.quoteTokenValue.roundQuantity(price);
        return {
            sizeToken: this.baseTokenValue.token,
            priceToken: this.quoteTokenValue.token,
            size: roundedDeltaX,
            totalCost,
            price: roundedPrice
        };
    }

    costForSellingBaseProportion(proportion) {
        const deltaX = this.baseTokenValue.value.times(proportion);
        return this.costForSellingBaseValue(deltaX);
    }

    costForBuyingQuoteValue(deltaY) {
        const roundedDeltaY = this.quoteTokenValue.roundQuantity(deltaY);
        if (numbers.Zero.eq(roundedDeltaY)) {
            log.warn(`Rounded sell size of ${deltaY} for ${this.quoteTokenValue.token} is zero.`);
            return null;
        }

        const newBalances = calculateUpdatedBalances(
            this.quoteTokenValue.value,
            this.baseTokenValue.value,
            roundedDeltaY,
            this.fee
        );
        const totalCost = this.baseTokenValue.value.minus(newBalances.y);
        const price = totalCost.div(roundedDeltaY);
        const roundedPrice = this.baseTokenValue.roundQuantity(price);
        return {
            sizeToken: this.quoteTokenValue.token,
            priceToken: this.baseTokenValue.token,
            size: deltaY,
            totalCost,
            price: roundedPrice
        };
    }

    costForBuyingQuoteProportion(proportion) {
        const deltaY = this.quoteTokenValue.value.times(proportion);
        return this.costForBuyingQuoteValue(deltaY);
    }

    costForSellingQuoteValue(deltaY) {
        const roundedDeltaY = this.quoteTokenValue.roundQuantity(deltaY);
        if (numbers.Zero.eq(roundedDeltaY)) {
            log.warn(`Rounded sell size of ${deltaY} for ${this.quoteTokenValue.token} is zero.`);
            return null;
        }

        const fee = numbers.negate(this.fee);
        const newBalances = calculateUpdatedBalances(
            this.quoteTokenValue.value,
            this.baseTokenValue.value,
            numbers.negate(roundedDeltaY),
            fee
        );

        // totalCost comes back negative when selling.
        const totalCost = numbers.negate(this.baseTokenValue.value.minus(newBalances.y));
        const price = totalCost.div(roundedDeltaY);
        const roundedPrice = this.baseTokenValue.roundQuantity(price);
        return {
            sizeToken: this.quoteTokenValue.token,
            priceToken: this.baseTokenValue.token,
            size: deltaY,
            totalCost,
            price: roundedPrice
        };
    }

    costForSellingQuoteProportion(proportion) {
        const deltaY = this.quoteTokenValue.value.times(proportion);
        return this.costForSellingQuoteValue(deltaY);
    }
}

module.exports = {
    calculateUpdatedBalances,
    ConstantProductCalculator
};