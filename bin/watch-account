#! /usr/bin/env node

"use strict";

const ctx = require("../lib/context.js");
const ensureArray = require("ensure-array");
const layouts = require("../lib/layouts.js");
const log = require("../lib/log.js");
const process = require("process");
const solana = require("@solana/web3.js");

async function main() {
    const context = await ctx.loadContext(process.argv);
    const accountStrings = ensureArray(context.account);
    for (const accountString of accountStrings) {
        const account = new solana.PublicKey(accountString);
        log.print(`Watching: [${account}]`);
        context.connection.onAccountChange(account, (something) => {
            try {
                const decoded = layouts.decodeInnerAccount(something);
                log.print(`From [${account}]:
    Owner:    ${decoded.owner}
    Mint:     ${decoded.mint}
    Amount:   ${decoded.amount}
    Lamports: ${decoded.lamports}
`);
            } catch (ex) {
                log.error("Caught exception:", ex);
            }
        });
    }
}

main().catch((ex) => log.critical(ex));